//includes by windowmanager
#include <math.h>
#include <stdlib.h>
#include<stdio.h>
#include <string.h>
#include <GL/gl.h>
#include "DNA_listBase.h"	
#include "DNA_screen_types.h"
#include "DNA_windowmanager_types.h"
#include "RNA_access.h"

#include "MEM_guardedalloc.h"
#include "GHOST_C-api.h"

#include "BLI_math.h"
#include "BLI_blenlib.h"
#include "BLI_utildefines.h"

#include "BLF_translation.h"
#include "BKE_blender.h"
#include "BKE_context.h"
#include "BKE_library.h"
#include "BKE_global.h"
#include "BKE_main.h"


//#include "BIF_gl.h"

#include "WM_api.h"
#include "WM_types.h"
#include "wm.h"
#include "wm_draw.h"
#include "wm_window.h"
#include "wm_subwindow.h"
#include "wm_event_system.h"

#include "ED_screen.h"
#include "ED_fileselect.h"

#include "PIL_time.h"

#include "GPU_draw.h"
#include "GPU_extensions.h"

#include "UI_interface.h"
#include "BLF_api.h"
//#include <ft2build.h>
//#include FT_FREETYPE_H
//#include FT_GLYPH_H


// used to set item color
typedef struct Color{
	short r, g, b;
}Color;


// decodes 
const float * decodeRgb(Color);
Color drawingColor;

/*enum {
	UI_CNR_TOP_LEFT = 1,
	UI_CNR_TOP_RIGHT = 2,
	UI_CNR_BOTTOM_RIGHT = 4,
	UI_CNR_BOTTOM_LEFT = 8,*/
	/* just for convenience */
	/*UI_CNR_NONE = 0,
	UI_CNR_ALL = (UI_CNR_TOP_LEFT | UI_CNR_TOP_RIGHT | UI_CNR_BOTTOM_RIGHT | UI_CNR_BOTTOM_LEFT)
};*/
static int roundboxtype = UI_CNR_ALL;
//end of includes by windowmanager
Global G;
UserDef U;
static void mainwindow_do_key(GHOST_TKey key);
//function comes from blender.c used to init globals
void initglobals(void)
{
	memset(&G, 0, sizeof(Global));
	
	U.savetime = 1;

	G.main = MEM_callocN(sizeof(Main), "initglobals");
	drawingColor.r = 0;
	drawingColor.g = 0;
	drawingColor.b = 0;
	strcpy(G.ima, "//");

#ifdef _WIN32   // FULLSCREEN
	G.windowstate = G_WINDOWSTATE_USERDEF;
#endif

	G.charstart = 0x0000;
	G.charmin = 0x0000;
	G.charmax = 0xffff;

#ifndef WITH_PYTHON_SECURITY /* default */
	G.f |= G_SCRIPT_AUTOEXEC;
#else
	G.f &= ~G_SCRIPT_AUTOEXEC;
#endif
}

void fdrawbox(float x1, float y1, float x2, float y2);
void sdrawbox(short x1, short y1, short x2, short y2);
/*windowmanager code*/
struct bScreen;
struct wmOperator;
char * buffer;

/* *************** internal api ************** */
void		wm_ghost_init			(bContext *C);
void		wm_ghost_exit(void);

/* the global to talk to ghost */
static GHOST_SystemHandle g_system = NULL;
/*end of windowmanager code*/
typedef enum WinOverrideFlag {
	WIN_OVERRIDE_GEOM     = (1 << 0),
	WIN_OVERRIDE_WINSTATE = (1 << 1)
} WinOverrideFlag;
static struct WMInitStruct {
	/* window geometry */
	int size_x, size_y;
	int start_x, start_y;

	int windowstate;
	WinOverrideFlag override_flag;
	
	int native_pixels;
} wm_init_state = {0, 0, 0, 0, GHOST_kWindowStateNormal, 0, 1};

/*main defines*/
void run_ghost_windowEx();
void timerProc(GHOST_TimerTaskHandle hTask, GHOST_TUns64 time);
int processEvent(GHOST_EventHandle hEvent, GHOST_TUserDataPtr userData);
static void setViewPortGL(GHOST_WindowHandle hWindow);
void uiDrawBox(int mode, float minx, float miny, float maxx, float maxy, float rad);
void drawSquare();
//void sdrawtrifill(short x1, short y1, short x2, short y2);
void sdrawtrifill(short x1, short y1, short x2, short y2);
static void sdrawtripoints(short x1, short y1, short x2, short y2);
void fdrawcheckerboard(float x1, float y1, float x2, float y2);

int main( int argc, char ** argv )
{
  printf("-----------------------------Memory-Test-------------------------\n");
  run_memory_test();
  printf("-----------------------------Enf-of-Memory-Test------------------\n");
  printf("-----------------------------GHOST-test-------------------------\n");
  //run_ghost_window();
  printf("-----------------------------Windowmanager-Test------------------\n");
  initglobals();
  bContext * bc;
  wm_ghost_init(bc);
  run_ghost_windowEx();
  printf("-----------------------------Enf-of-Windowmanager-Test-----------\n");

  return 0;
}
GHOST_WindowHandle gwh = NULL;
GHOST_TimerTaskHandle timer;
int sExit = 1;
/*windowmanager source */
void wm_ghost_init(bContext *C)
{
	if (!g_system) {
		// temporary
		//GHOST_EventConsumerHandle consumer = GHOST_CreateEventConsumer(ghost_event_proc, C);
		
		g_system = GHOST_CreateSystem();
		//GHOST_AddEventConsumer(g_system, consumer);

		
		if (wm_init_state.native_pixels) {
			GHOST_UseNativePixels();
		}
	}
}

void wm_ghost_exit(void)
{
	if (g_system)
		GHOST_DisposeSystem(g_system);

	g_system = NULL;
}
/*end of windowmanager source */
/*other source*/
void run_ghost_windowEx()
{
	buffer = calloc(10, sizeof(char));
	buffer[0] = 'a';
	buffer[1] = 'b';
	buffer[2] = 'c';
	buffer[3] = 'd';
	buffer[4] = 'e';
	buffer[5] = 'f';
	buffer[6] = 'g';
	buffer[7] = 'h';
	buffer[8] = 'i';
	buffer[9] = 'j';

		gwh = GHOST_CreateWindow(g_system,
		                                 "Ghost test",
		                                 10,
		                                 64,
		                                 320,
		                                 200,
		                                 GHOST_kWindowStateNormal,
		                                 GHOST_kDrawingContextTypeOpenGL,
		                                 FALSE,
		                                 FALSE);
		/* Install a timer to have the gears running */
		timer = GHOST_InstallTimer(g_system,
		                                 0,
		                                 1000,
		                                 timerProc,
		                                 gwh);

		GHOST_EventConsumerHandle consumer = GHOST_CreateEventConsumer(processEvent, NULL);
		GHOST_AddEventConsumer(g_system, consumer);

		while (sExit)
		{
			if (!GHOST_ProcessEvents(g_system, 0)) 
			{
#ifdef WIN32
				/* If there were no events, be nice to other applications */
				Sleep(10);
#endif
			}
			GHOST_DispatchEvents(g_system);
		}
}

void timerProc(GHOST_TimerTaskHandle hTask, GHOST_TUns64 time)
{
	GHOST_WindowHandle hWindow = NULL;
	//fAngle += 2.0;
	//view_roty += 1.0;
	hWindow = (GHOST_WindowHandle)GHOST_GetTimerTaskUserData(hTask);
	if (GHOST_ValidWindow(g_system, hWindow))
	{
		//printf("valid window \n");
		GHOST_InvalidateWindow(hWindow);
	}
}

int processEvent(GHOST_EventHandle hEvent, GHOST_TUserDataPtr userData)
{
	int handled = 1;

	switch (GHOST_GetEventType(hEvent))
	{

		case GHOST_kEventWindowClose:
		{
			printf("Closing window\n");
			GHOST_WindowHandle window2 = GHOST_GetEventWindow(hEvent);
			if (window2)
			{
				sExit = 0;
			}
			else {
				if (timer)
				{
					GHOST_RemoveTimer(g_system, timer);
					timer = 0; // why?
				}
				GHOST_DisposeWindow(g_system, window2);
			}
			
		}
		break;
		case GHOST_kEventKeyDown:
		case GHOST_kEventKeyUp:
		{
			GHOST_TEventKeyData *kd = GHOST_GetEventData(hEvent);
			mainwindow_do_key(kd->key);
			break;
		}


		case GHOST_kEventWindowUpdate:
		{
			GHOST_WindowHandle window2 = GHOST_GetEventWindow(hEvent);
			if (!GHOST_ValidWindow(g_system, window2))
				break;
			setViewPortGL(window2);
			//sdrawbox(-1.0f, 1.0f, 1.0f, -1.0f);
			//sdrawtripoints(-1, 1, 1, -1); //did not 
			//fdrawcheckerboard(-1.0f, 1.0f, 1.0f, -1.0f);
			glColor3fv(decodeRgb(drawingColor));
	 		//glColor3f(0.0f, 0.0f, 1.0f); // Colour our shape blue  
			uiDrawBox(GL_POLYGON, 0.0 , 0.0, 1.0f, 1.0f, 0.25);
			//glColor3f(1.0f, 1.0f, 0.0f); // Colour our shape blue  
			//uiDrawBox(GL_POLYGON, 0.0, 0.0, -1.0f, -1.0f, -0.
			//BLF_draw_default(0.0, 0.0, 0.0, "text", 11);
			GHOST_SwapWindowBuffers(window2);
			//void uiDrawBox(int mode, float minx, float miny, float maxx, float maxy, float rad);
		}
		break;
		default:
			handled = 0;
		break;
	}
	return handled;
}

static void setViewPortGL(GHOST_WindowHandle hWindow)
{
	GHOST_RectangleHandle hRect = NULL;
	GLfloat w, h;
	
	GHOST_ActivateWindowDrawingContext(hWindow);
	hRect = GHOST_GetClientBounds(hWindow);
	
	w = (float)GHOST_GetWidthRectangle(hRect) / (float)GHOST_GetHeightRectangle(hRect);
	h = 1.0;
	
	glViewport(0, 0, GHOST_GetWidthRectangle(hRect), GHOST_GetHeightRectangle(hRect));
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-w, w, -h, h, 5.0, 60.0);
	/* glOrtho(0, bnds.getWidth(), 0, bnds.getHeight(), -10, 10); */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, 0.0, -40.0);
	
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	GHOST_DisposeRectangle(hRect);
}

void drawSquare()
{  
    glColor3f(0.0f, 0.0f, 1.0f); // Colour our shape blue  
    glBegin(GL_QUADS); // Start drawing a quad 
    glVertex3f(-1.0f, -1.0f, 0.0f); // The bottom left corner  
    glVertex3f(-1.0f, 1.0f, 0.0f); // The top left corner  
    glVertex3f(1.0f, 1.0f, 0.0f); // The top right corner  
    glVertex3f(1.0f, -1.0f, 0.0f); // The bottom right corner  
    glEnd();  
}

void fdrawbox(float x1, float y1, float x2, float y2)
{
	float v[2];
	
	glBegin(GL_LINE_STRIP);
	
	v[0] = x1; v[1] = y1;
	glVertex2fv(v);
	v[0] = x1; v[1] = y2;
	glVertex2fv(v);
	v[0] = x2; v[1] = y2;
	glVertex2fv(v);
	v[0] = x2; v[1] = y1;
	glVertex2fv(v);
	v[0] = x1; v[1] = y1;
	glVertex2fv(v);
	
	glEnd();
}

void sdrawbox(short x1, short y1, short x2, short y2)
{
	short v[2];
	
	glBegin(GL_LINE_STRIP);
	
	v[0] = x1; v[1] = y1;
	glVertex2sv(v);
	v[0] = x1; v[1] = y2;
	glVertex2sv(v);
	v[0] = x2; v[1] = y2;
	glVertex2sv(v);
	v[0] = x2; v[1] = y1;
	glVertex2sv(v);
	v[0] = x1; v[1] = y1;
	glVertex2sv(v);
	
	glEnd();
}

void sdrawtrifill(short x1, short y1, short x2, short y2)
{
	glBegin(GL_TRIANGLES);
	sdrawtripoints(x1, y1, x2, y2);
	glEnd();
}

static void sdrawtripoints(short x1, short y1, short x2, short y2)
{
	short v[2];
	//glColor3f(0.0f, 0.0f, 1.0f); // Colour our shape blue  
	v[0] = x1; v[1] = y1;
	glVertex2sv(v);
	v[0] = x1; v[1] = y2;
	glVertex2sv(v);
	v[0] = x2; v[1] = y1;
	glVertex2sv(v);
}

void fdrawcheckerboard(float x1, float y1, float x2, float y2)
{
	unsigned char col1[4] = {40, 40, 40}, col2[4] = {50, 50, 50};

	GLubyte checker_stipple[32 * 32 / 8] = {
		255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0,
		255,  0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0,
		0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255,
		0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255,
		255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0,
		255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0,
		0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255,
		0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255};
	
	glColor3ubv(col1);
	glRectf(x1, y1, x2, y2);
	glColor3ubv(col2);

	glEnable(GL_POLYGON_STIPPLE);
	glPolygonStipple(checker_stipple);
	glRectf(x1, y1, x2, y2);
	glDisable(GL_POLYGON_STIPPLE);
}


void uiDrawBox(int mode, float minx, float miny, float maxx, float maxy, float rad)
{
	float vec[7][2] = {{0.195, 0.02}, {0.383, 0.067}, {0.55, 0.169}, {0.707, 0.293},
	                   {0.831, 0.45}, {0.924, 0.617}, {0.98, 0.805}};
	int a;

	/* mult */
	for (a = 0; a < 7; a++) {
		mul_v2_fl(vec[a], rad);
	}

	glBegin(mode);

	/* start with corner right-bottom */
	if (roundboxtype & UI_CNR_BOTTOM_RIGHT) {
		glVertex2f(maxx - rad, miny);
		for (a = 0; a < 7; a++) {
			glVertex2f(maxx - rad + vec[a][0], miny + vec[a][1]);
		}
		glVertex2f(maxx, miny + rad);
	}
	else glVertex2f(maxx, miny);
	
	/* corner right-top */
	if (roundboxtype & UI_CNR_TOP_RIGHT) {
		glVertex2f(maxx, maxy - rad);
		for (a = 0; a < 7; a++) {
			glVertex2f(maxx - vec[a][1], maxy - rad + vec[a][0]);
		}
		glVertex2f(maxx - rad, maxy);
	}
	else glVertex2f(maxx, maxy);
	
	/* corner left-top */
	if (roundboxtype & UI_CNR_TOP_LEFT) {
		glVertex2f(minx + rad, maxy);
		for (a = 0; a < 7; a++) {
			glVertex2f(minx + rad - vec[a][0], maxy - vec[a][1]);
		}
		glVertex2f(minx, maxy - rad);
	}
	else glVertex2f(minx, maxy);
	
	/* corner left-bottom */
	if (roundboxtype & UI_CNR_BOTTOM_LEFT) {
		glVertex2f(minx, miny + rad);
		for (a = 0; a < 7; a++) {
			glVertex2f(minx + vec[a][1], miny + rad - vec[a][0]);
		}
		glVertex2f(minx + rad, miny);
	}
	else glVertex2f(minx, miny);
	
	glEnd();
}

const float * decodeRgb(Color c)
{
	float * r = calloc(3, sizeof(float));
	printf("Given colors %d %d %d \n", c.r, c.g, c.b);
	r[0] = (c.r/255.0); 
	r[1] = (c.g/255.0);
	r[2] = (c.b/255.0);
	printf("decoded colors %f %f %f \n", r[0], r[1], r[2]);
	return r;
}

static void mainwindow_do_key(GHOST_TKey key)
{

	switch(key)
	{

		case GHOST_kKeyR:
		{
			drawingColor.r = drawingColor.r + 1 ;
			printf("Updating red color\n");
			printf("Current color %d %d %d \n", drawingColor.r, drawingColor.g, drawingColor.b);
			
		}
		break;
		case GHOST_kKeyG:
		{
			drawingColor.g = drawingColor.g + 1 ;
			printf("Updating green color\n");
			printf("Current color %d %d %d \n", drawingColor.r, drawingColor.g, drawingColor.b);
			
		}
		break;
		case GHOST_kKeyB:
		{
			drawingColor.b = drawingColor.b + 1 ;
			printf("Updating blue color \n");
			printf("Current color %d %d %d \n", drawingColor.r, drawingColor.g, drawingColor.b);
		}
		break;
	}
}
