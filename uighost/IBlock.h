#ifndef IBLOCK_H
#define IBLOCK_H

#include <list>
//#include "box_layout.h" // removed ILayout should be used
#include "ILayout.h"
#include "ui_rect.h"
#include "font_api.h"
#include "event_manager.h"
#include "ui_border.h"
#include "ui_insets.h"

/*
Block model that represents current block usage

   <-------------calculated-width--------------->
                <------width--------->
.  ..............................................
:  :         (1)Margin                          :
c  :    ---------(2)Border------------------    :
a  :(1) |               (3)Padding         | (1): 
l  :    |       This is the Content        |    : 
   :   (2)      of the element "box"      (2)   : 
w  :    |       that represents each       |    : 
i  :    |  (3)  element.              (3)  |    : 
d  :    |                 (3)              |    :
t  :    ------------(2)---------------------    :
h  :           (1)                              :
.  :............................................:

*/
namespace UI{
	//forwards
	class Window;// cannot use include because window includes IBlock
	typedef boost::weak_ptr<Window> WindowWPtr; 
	typedef boost::shared_ptr<Window> WindowPtr;// used when locking weak_ptr

	class IBlock;
	typedef boost::shared_ptr<IBlock> IBlockPtr;

	class UIRect;
	typedef boost::shared_ptr<Layout::ILayout> ILayoutPtr;

	class IBlock{
	protected:
		UI::WindowWPtr m_wpWindow; // weak pointer to it's window to be able to get window event manager and other things block may need
		Color m_BackgroundColor;
		Insets m_Insets;

	public:
		virtual
		~IBlock(){};

		virtual void 
		addChild(IBlock * child) = 0;

		virtual UI::Layout::ILayout * 
		getLayout() = 0;

		virtual void  
		setLayout(UI::Layout::ILayout * layout) = 0;

		virtual bool 
		invalidate() = 0; // recalculate position of the block
	
		virtual bool 
		invalidateChildren() = 0;

		virtual std::list<IBlock *> * 
		getChildren() = 0; 

		virtual bool 
		draw() = 0;

		virtual UIRect
		getMinimumSize() = 0;

		virtual UIRect
		getMaximumSize() = 0;

		virtual UIRect
		getPreferredSize() = 0;

		virtual void
		setMinimumSize(UIRect size) = 0;

		virtual void 
		setMaximumSize(UIRect size) = 0;

		virtual void 
		setPreferredSize(UIRect size) = 0;

		virtual UIRect 
		getPosition() = 0;

		virtual void
		setPosition(UIRect position) = 0;

//common methods that are not virtual
		void 
		setBackgroundColor(Color c)
		{
			m_BackgroundColor = c;
		}

		Color 
		getBackgroundColor(){
			return m_BackgroundColor;	
		}

		Padding
		getPadding(){return m_Insets.getPadding();}

		Border
		getBorder(){return m_Insets.getBorder();}

		void 
		setBorder(Border border)
		{
			m_Insets.setBorder(border);
		}

		void 
		setPadding(Padding padding)
		{		
			m_Insets.setPadding(padding);
		}

		void 
		setMargin(Margin margin)
		{
			m_Insets.setMargin(margin);
		}

		Margin
		getMargin()
		{
			return m_Insets.getMargin();
		}

		UI::Insets
		getInsets()
		{
			return m_Insets;		
		}
		IBlock(WindowWPtr wpWindow) : m_wpWindow(wpWindow){}

		WindowWPtr
		getWindow(){ return m_wpWindow;}
	};
}
#endif
