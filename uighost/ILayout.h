#include <list>
#include <math.h>
#include <iostream>
#include "ui_rect.h"

#ifndef UI_ILAYOUT_H
#define UI_ILAYOUT_H

/**
 * This class manages how box will be placed on screen
 */
namespace UI{
	class IBlock; // veird forward because class UI::IBlock; is not allowed by c++ standart
 
	namespace Layout{

		class ILayout{
		public:
			/**
			 * Method calculates block size
			 * This should be used after event which will cause block to change it's size
			 * TODO calculate spacing
			 */		
			virtual	bool 
			invalidate() = 0;
			
			/**
			 * Method draws all layout managed blocks
			 */
			virtual void 
			drawLayout() = 0;

			virtual void 
			setPositioning(int p) = 0;

			virtual void
			addBlock(IBlock * block) = 0;

			/*
			 * This method has to reset inner positions of layout
			 * This will be used when layout manager is changed
			 * Some default behaveour will be implemented how to place new items if there is any
			 */
			virtual void
			reset() = 0;
 
			ILayout(UI::IBlock * b) : block(b) 
			{}
		protected:
			UI::IBlock * block; // TODO remove this use m_Block instead
			UI::IBlock * m_pBlock;
			int positioning;
		};
	}
}
#endif
