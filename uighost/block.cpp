#include "block.h"
#include "event_manager.h"
#include "ui_window.h" // might be not safe

using namespace UI;

Block::Block(WindowWPtr wpWindow) : IBlock(wpWindow)
{
	this->preferredSize = UIRect(Block::DEFAULT_MIN_WIDTH, Block::DEFAULT_MIN_HEIGHT);
	this->minimumSize = UIRect(0, 0);
	this->maximumSize = UIRect(Block::MAX_WIDTH, Block::MAX_HEIGHT);
	this->spacing = Spacing::NONE;
	this->initBlock();
}

Block::Block(WindowWPtr wpWindow, UIRect preferredSize) : IBlock(wpWindow)
{
	this->preferredSize = preferredSize;
	this->minimumSize = UIRect(0, 0);
	this->maximumSize = UIRect(Block::MAX_WIDTH, Block::MAX_HEIGHT);
	this->spacing = Spacing::NONE;
	this->initBlock();
}


Block::Block(WindowWPtr wpWindow, UIRect preferredSize, int spacing) : IBlock(wpWindow)
{
	this->preferredSize = preferredSize;
	this->minimumSize = UIRect(0, 0);
	this->maximumSize = UIRect(Block::MAX_WIDTH, Block::MAX_HEIGHT);
	this->spacing = spacing;
	this->initBlock();
}

Block::Block(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, int spacing) : IBlock(wpWindow)
{
	this->preferredSize = preferredSize;
	this->minimumSize = minimumSize;
	this->maximumSize = UIRect(Block::MAX_WIDTH, Block::MAX_HEIGHT);
	this->spacing = spacing;
	this->initBlock();
}

Block::Block(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, UIRect maximumSize, int spacing) : IBlock(wpWindow)
{
	this->preferredSize = preferredSize;
	this->minimumSize = minimumSize;
	this->maximumSize = maximumSize;
	this->spacing = spacing;
	this->initBlock();
}

void Block::initBlock()
{
	this->children = new std::list<IBlock *>(); // create empty list
	this->position = UIRect();
	this->layout = nullptr; // pointer default value
}
// get methods

UIRect Block::getMinimumSize()
{
	return this->minimumSize;
}

UIRect Block::getPreferredSize()
{
	return this->preferredSize;
}

UIRect Block::getMaximumSize()
{
	return this->maximumSize;
}

std::list<IBlock*> * Block::getChildren()
{
	//printf("Returning list on %p", (void *)children);
	return this->children;
}

void Block::addChild(IBlock * child)
{
	//printf("Getting child %p  \n", (void *)child);
	
	//printf("List is : %p \n", (void *)children);
	// list was not created
	if(this->children == nullptr)
	{
		printf("List was not created \n");
	} else
	{
		this->children->push_back(child);
	}
}

// set methods
void Block::setPreferredSize(UIRect size)
{
	this->preferredSize = size;
}

void Block::setMaximumSize(UIRect size)
{
	this->maximumSize = size;
}

void Block::setMinimumSize(UIRect size)
{
	this->minimumSize = size;
}

Block::~Block()
{
	delete this->children;
}

std::string Block::toString()
{
	stringstream ss;
	ss << " minimumSize: " << minimumSize.toString() << " preferredSize: " << preferredSize.toString()
	   << " maximumSize: " << maximumSize.toString() << " position: " << position.toString() << std::endl;
	return ss.str();
}

void Block::setLayout(Layout::ILayout * layout)
{
//std::cout << "Current block layout is: "<< static_cast<void *>(this->layout) << std::endl;
//std::cout << "Layout is null: " << ((this->layout == nullptr) ? "true" : "false") << std::endl;
	if(this->layout != nullptr)
	{
		delete this->layout; // free memory
	}
	
	this->layout = layout;
	// now layout has to be reset to place items correctly 
	// this is needed when we change layout manager like from boxlayout to border so borderlayout has to update itself
	this->layout->reset();
}

Layout::ILayout * Block::getLayout()
{
	if(this->layout == nullptr)
	{
		this->layout = new Layout::BoxLayout(this);
	}

	return this->layout;
}

bool Block::invalidate()
{
	//std::cout << "Block::invalidate() starting invalidation " << std::endl;
	if(layout == nullptr)
	{
//std::cout << "Block::invalidate() created default layout " << std::endl;
		layout = new Layout::BoxLayout(this);
	}
	
	if(!layout->invalidate()) // invalidating current block
	{
		printf("Block::%s() Could not invalidate block\n", __func__);
		return false;
	} else {
		//std::cout << "Block::invalidate() invalidated itself successfully " << std::endl;
	}
	
	if(!this->invalidateChildren())
	{
		printf("Block::%s() Could not invalidate children\n", __func__);
		return false;
	} else {
		//std::cout << "Block::invalidate() invalidated children successfully " << std::endl;
	}
	
	return true;
}

bool Block::invalidateChildren()
{
//std::cout << "Block::invalidateChildren() starting children invalidation " << std::endl;
	bool success = true;
	
	if(children != nullptr)
	{
		for(std::list<IBlock *>::iterator it  = children->begin(); it != children->end(); it++)
		{
			success = ((*it)->invalidate() && success); // if invalidate returns false then success allways will be false;
		}
	}
	
	return success;
}

void Block::setPosition(UIRect rect)
{
	this->position = rect;
}

UIRect Block::getPosition()
{
	return this->position;
}

bool Block::draw()
{
	WindowPtr pWindow; // window handle
	bool success = true;

	if(!(pWindow = m_wpWindow.lock()))
	{
		printf("Block::%s() could not lock window \n", __func__);
		return false;
	}

	//TODO add support for rounded corners

	success = drawBackground() && success;
	success = drawBorder() && success;

	return success;
}	


bool 
Block::drawBackground()
{
	WindowPtr pWindow; // window handle

	Margin blockMargin = getMargin();
	//Padding blockPadding = getPadding();
	Border blockBorder = getBorder();

	if(!(pWindow = m_wpWindow.lock()))
	{
		printf("Block::%s() could not lock window \n", __func__);
		return false;
	}

	int width = pWindow->getWindowWidth();
	int height = pWindow->getWindowHeight();

	UIRect blockPosition = getPosition();

	int x1 = blockPosition.coordX + blockMargin.left + blockBorder.getThickness();
	int y1 = blockPosition.coordY + blockMargin.top + blockBorder.getThickness();
	int x2 = blockPosition.coordX + blockPosition.width - blockMargin.right - blockBorder.getThickness();
	int y2 = blockPosition.coordY + blockPosition.height - blockMargin.bottom - blockBorder.getThickness();

	// setting color to draw background
	glColor3fv(decodeRGBA(&m_BackgroundColor));
	// drawing
	drawBox(GL_POLYGON, width, height, x1, y1 , x2, y2);
	return true;
}

bool
Block::drawBorder()
{
	WindowPtr pWindow; // window handle

	Margin blockMargin = getMargin();
	//Padding blockPadding = getPadding();
	Border blockBorder = getBorder();

	if(!(pWindow = m_wpWindow.lock()))
	{
		printf("Block::%s() could not lock window \n", __func__);
		return false;
	}

	int width = pWindow->getWindowWidth();
	int height = pWindow->getWindowHeight();

	UIRect blockPosition = getPosition();

	int x1 = blockPosition.coordX + blockMargin.left;
	int y1 = blockPosition.coordY + blockMargin.top;
	int x2 = blockPosition.coordX + blockPosition.width - blockMargin.right;
	int y2 = blockPosition.coordY + blockPosition.height - blockMargin.bottom;

	//drawing border
	Color borderColor = blockBorder.getColor();
	glColor3fv(decodeRGBA(&borderColor));

	UI::drawBorder(GL_POLYGON, width, height, x1, y1, x2, y2, blockBorder.getThickness());
	return true;
}

void Block::beforeDraw()
{
	//std::list<IBlock *> * children = this->getChildren();

	/*if((children != nullptr)&&(children->size() > 0))
	{
		for(std::list<IBlock *>::iterator it  = children->begin(); it != children->end(); it++)
		{
			// todo something with children if needed
		}
	}*/
}


