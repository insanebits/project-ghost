#ifndef BLOCK_H
#define BLOCK_H
#include "IBlock.h"

//#include <GL/glew.h> // should be together
//#include <GL/gl.h> // openGL

#include "ui_types.h" // first loading types because many classes may need it
#include "box_layout.h"
#include "ui_rect.h"
#include "ui_drawing.h"
#include "font_api.h"// might be removed in future

namespace UI{
	using namespace std; // TODO remove this

	
	/* Block is constructing block for UI
	 * It defines most basic block parameters like size 
	 * Each block has it's own layout which will be used to represent(draw) block
	*/
	class Block : public IBlock{
	public:
		static const int MAX_WIDTH = 1920*8; // 8k HD
		static const int MAX_HEIGHT = 1080*8; 
		static const int DEFAULT_MIN_WIDTH = 1;
		static const int DEFAULT_MIN_HEIGHT = 1;
		
		/*initialization of block inner data*/
		void initBlock();
		Block(WindowWPtr wpWindow, UIRect preferredSize, int spacing);
		Block(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, int spacing);
		Block(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, UIRect maximumSize, int spacing);
		Block(WindowWPtr wpWindow, UIRect preferredSize);
		Block(WindowWPtr wpWindow);
		
		void addChild(IBlock * child) override;
		std::list<IBlock *> * getChildren() override; 

		UIRect getMinimumSize();
		UIRect getMaximumSize();
		UIRect getPreferredSize();
		Layout::ILayout * getLayout() override;
		
		
		// set methods
		void setPreferredSize(UIRect rect) override;
		void setMaximumSize(UIRect rect) override;
		void setMinimumSize(UIRect rect) override;
		void setLayout(Layout::ILayout * layout) override;
		//void setFontManager(FontManagerPtr pFontManager) override;
		~Block();

		//bool hasFontManager() override; // to check if block has font manager
		// debug
		std::string toString();
		
		bool 
		draw() override;

		bool 
		invalidate() override; // recalculate position of the block

		void 
		setPosition(UIRect rect);

		UIRect
		getPosition();

	protected:
		std::list<IBlock *>* children; // block can have child blocks
		//FontManagerPtr m_pFontManager;// removed use block window
		UIRect preferredSize;
		UIRect maximumSize;
		UIRect minimumSize;
		// block position on screen
		UIRect position; // layout is responsible to set calculate corrent position of the block
		int spacing;

		Layout::ILayout * layout;
		//Margin margin;

		bool 
		drawBackground();

		bool
		drawBorder();

		
		// recalculate position of the chilren 
		//usually it will call each child invalidate method
		bool invalidateChildren(); 
		void beforeDraw();
		//TODO expand ablility to have more than one type of layout
	};
}

#endif
