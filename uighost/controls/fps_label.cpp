#include "fps_label.h"
using namespace UI::Controls;

FPSLabel::FPSLabel(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, UIRect maximumSize, int spacing):
	Label(wpWindow, preferredSize, minimumSize, maximumSize, spacing)
{		
	m_LastRepaint = microsec_clock::local_time(); // on creation we set when 
	m_RepaintCount = 0; // it will be incremented when draw method is called
	m_Text = "0";
}

FPSLabel::FPSLabel(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, int spacing):
	Label(wpWindow, preferredSize, minimumSize, spacing)
{			
	m_LastRepaint = microsec_clock::local_time(); // on creation we set when 
	m_RepaintCount = 0; // it will be incremented when draw method is called
	m_Text = "0";
}

bool
FPSLabel::draw()
{
	ptime now = microsec_clock::local_time();
	time_duration duration = now - m_LastRepaint;
	//printf("Total miliseconds %d \n", duration.total_milliseconds());
	if(duration.total_milliseconds() < 1000) // second
	{
		// second not passed so we increment repaint count
		m_RepaintCount++;	
	} else {
		//printf("Window repainted '%d' times in '%f' seconds\n", m_RepaintCount, seconds);
		m_Text = std::to_string(m_RepaintCount);	
		m_RepaintCount = 0;
		m_LastRepaint = now;
	}
	return Label::draw();
}


void 
FPSLabel::handleMouseEvent(Events::MouseEventDataPtr &medp)
{
	if(m_BackgroundColor.red < 255)
	{
		m_BackgroundColor.red++;
	} else {
		m_BackgroundColor.red = 0;
	}
}
