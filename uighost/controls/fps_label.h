#ifndef FPS_LABEL_H
#define FPS_LABEL_H

#include "label.h"
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>


namespace UI{
	namespace Controls{

		using namespace boost::posix_time;
		
		class FPSLabel: public Label{
		private:
			ptime m_LastRepaint;
			int m_RepaintCount;
	
		public:			
			FPSLabel(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, UIRect maximumSize, int spacing);
		
			FPSLabel(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, int spacing);
			 
			bool
			draw();

			void 
			handleMouseEvent(Events::MouseEventDataPtr &medp);
		};
	}
}
#endif
