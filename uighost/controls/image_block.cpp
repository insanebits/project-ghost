#include "image_block.h"
#include "ui_window.h"// might be dangerous 
using namespace UI::Controls;

ImageBlock::ImageBlock(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, UIRect maximumSize, int spacing) : Label(wpWindow, preferredSize, minimumSize, maximumSize, spacing)
{}
ImageBlock::ImageBlock(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, int spacing) : Label(wpWindow, preferredSize, minimumSize, spacing)
{}

ImageBlock::ImageBlock(WindowWPtr wpWindow, UIRect preferredSize, int spacing) : Label(wpWindow, preferredSize, spacing)
{}

bool 
ImageBlock::draw()
{
	WindowPtr pWindow; // window handle

	Margin blockMargin = getMargin();
	Padding blockPadding = getPadding();
	Border blockBorder = getBorder();

	if(!(pWindow = m_wpWindow.lock()))
	{
		printf("Block::%s() could not lock window \n", __func__);
		return false;
	}

	int width = pWindow->getWindowWidth();
	int height = pWindow->getWindowHeight();

	int leftInset =  blockMargin.left + blockPadding.left + blockBorder.getThickness();
	int topInset =  blockMargin.top + blockPadding.top + blockBorder.getThickness();
	int rightInset = blockMargin.right + blockPadding.right + blockBorder.getThickness();
	int bottomInset = blockMargin.bottom + blockPadding.bottom + blockBorder.getThickness();

	//printf("Block insets %d %d %d %d \n", leftInset, topInset, rightInset, bottomInset);

	// drawing area that includes border, padding, margin
	int currentPointX = this->getPosition().coordX + leftInset;
	int currentPointY = this->getPosition().coordY + topInset;
	int drawToX = currentPointX + this->getPosition().width - rightInset;
	int drawToY = currentPointY + this->getPosition().height - bottomInset;


	//TODO add support for rounded corners

	Block::draw(); // will draw everything needed we only draw text
	// background image drawing
	if(m_pBackgroundBitmap)
	{
		UI::drawBitmap(m_pBackgroundBitmap, width, height, currentPointX, currentPointY, drawToX, drawToY); // 1 px each size smaller 
	}

	//text rendering
	GLCoordinate start(height, width, currentPointX, currentPointY + m_FontSize);
	GLCoordinate finish(width, height, drawToX, drawToY);
	
	FontManagerPtr pFontManager = pWindow->getFontManager();
	if(!pFontManager)
	{
		printf("Label::%s() could not get font manager instance\n", __func__);
		return false;
	}

	pFontManager->setCurrentFontColor(m_FontColor);
	pFontManager->setCurrentFont(m_FontName);
  pFontManager->setCurrentFontSize(m_FontSize);

	pFontManager->render_text(m_Text.c_str(), start, finish,  true, true);
	return true;
}

void 
ImageBlock::setBackgroundImage(std::string path)
{
	m_BackgroundImagePath = path;
	m_pBackgroundBitmap = new Bitmap(path.c_str());
}
