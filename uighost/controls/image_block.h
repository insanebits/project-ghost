#ifndef UI_CONTROLS_IMAGE_BLOCK_H
#define UI_CONTROLS_IMAGE_BLOCK_H
#include "label.h"
namespace UI{
	namespace Controls{
		class ImageBlock : public Label
		{
		public:
			ImageBlock(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, UIRect maximumSize, int spacing);
			
			ImageBlock(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, int spacing);
		
			ImageBlock(WindowWPtr wpWindow, UIRect preferredSize, int spacing);

			bool 
			draw();

			void 
			setBackgroundImage(std::string path);
		protected:
			std::string m_BackgroundImagePath;
			Bitmap * m_pBackgroundBitmap;

		};
	}
}
#endif
