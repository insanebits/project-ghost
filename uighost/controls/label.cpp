#include "label.h"
#include "ui_window.h"// might be dangerous 
using namespace UI::Controls;

void		
Label::setFont(std::string fontName)
{
	m_FontName = fontName;			
}

void
Label::setFontSize(int fontSize)
{
	m_FontSize = fontSize;			
}

void
Label::setText(std::string text)
{
	m_Text = text;
}

std::string
Label::getText()
{
	return m_Text;
}

void
Label::setFontColor(Color color)
{
	m_FontColor = color;
}

Label::Label(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, UIRect maximumSize, int spacing):
	Block(wpWindow, preferredSize, minimumSize, maximumSize, spacing)
{			
	init();
}

Label::Label(WindowWPtr wpWindow, UIRect minimumSize, int spacing):
	Block(wpWindow, preferredSize, spacing)
{			
	init();
}

Label::Label(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, int spacing):
	Block(wpWindow, preferredSize, minimumSize, spacing)
{
	init();			
}

bool 
Label::draw()
{
	WindowPtr pWindow; // window handle

	Margin blockMargin = getMargin();
	Padding blockPadding = getPadding();
	Border blockBorder = getBorder();

	if(!(pWindow = m_wpWindow.lock()))
	{
		printf("Block::%s() could not lock window \n", __func__);
		return false;
	}

	int width = pWindow->getWindowWidth();
	int height = pWindow->getWindowHeight();

	int leftInset =  blockMargin.left + blockPadding.left + blockBorder.getThickness();
	int topInset =  blockMargin.top + blockPadding.top + blockBorder.getThickness();
	int rightInset = blockMargin.right + blockPadding.right + blockBorder.getThickness();
	int bottomInset = blockMargin.bottom + blockPadding.bottom + blockBorder.getThickness();

	//printf("Block insets %d %d %d %d \n", leftInset, topInset, rightInset, bottomInset);

	// drawing area that includes border, padding, margin
	int currentPointX = this->getPosition().coordX + leftInset;
	int currentPointY = this->getPosition().coordY + topInset;
	int drawToX = currentPointX + this->getPosition().width - rightInset;
	int drawToY = currentPointY + this->getPosition().height - bottomInset;


	//TODO add support for rounded corners

	Block::draw(); // will draw everything needed we only draw text

	//text rendering
	GLCoordinate start(height, width, currentPointX, currentPointY + m_FontSize);
	GLCoordinate finish(width, height, drawToX, drawToY);
	
	FontManagerPtr pFontManager = pWindow->getFontManager();
	if(!pFontManager)
	{
		printf("Label::%s() could not get font manager instance\n", __func__);
		return false;
	}

	pFontManager->setCurrentFontColor(m_FontColor);
	pFontManager->setCurrentFont(m_FontName);
  pFontManager->setCurrentFontSize(m_FontSize);

	pFontManager->render_text(m_Text.c_str(), start, finish,  true, true);
	return true;
}	

void 
Label::handleKeyEvent(Events::KeyEventDataPtr &pKeyEventData){
	if(!pKeyEventData){
		printf("Not valid event data\n");
		return;
	}
}


void 
Label::handleEvent(Events::IEventProducerPtr &p, Events::IEventArgsPtr &a) {
	printf("Label::%s() handling event of '%s' type\n", __func__, p->getProducerType().c_str());

	if(p->getProducerType() == "MouseEvent")
	{
		Events::MouseEventDataPtr med = boost::dynamic_pointer_cast<Events::MouseEventData>(a);
		UIRect labelPosition = getPosition();

		int clickedX = med->getPositionX();
		int clickedY = med->getPositionY();
		WindowPtr pWindow;
		if(!(pWindow = getWindow().lock()))
		{
			printf("Label::%s() could not lock window");
			return; 
		}

		// TODO conversion should be in mouse driver or somewhere not so far
		pWindow->screenToClient(clickedX, clickedY, clickedX, clickedY); // converting to our window coordinates

		int labelStartX = labelPosition.coordX;
		int labelStartY = labelPosition.coordY;

		int labelEndX = labelStartX + labelPosition.width;
		int labelEndY = labelStartY + labelPosition.height;

		// only handle event if it's clicked on our block 
		if((clickedX >= labelStartX) && (clickedX <= labelEndX))
		{
			if((clickedY >= labelStartY) && (clickedY <= labelEndY))
			{
				handleMouseEvent(med);
			}
		}
		
		return;
	}

	if(p->getProducerType() == "KeyEvent")
	{
		Events::KeyEventDataPtr ked = boost::dynamic_pointer_cast<Events::KeyEventData>(a);
		//passing data to internal method
		handleKeyEvent(ked);
		
		return;
	}
	//KeyEventDataPtr ked = boost::dynamic_pointer_cast<KeyEventData>(a);
	// passing data to internal method
	//handleKeyEvent(ked);
}

void
Label::init()
{
	/*Events::IEventListenerPtr listener(this);
	WindowPtr pWindow;
	if(!(pWindow = m_wpWindow.lock()))
	{
		printf("TextBlock::%s() could not lock window\n", __func__);
		return;
	}	

	pWindow->getEventManager()->registerEventListener("MouseEvent", listener);
	pWindow->getEventManager()->registerEventListener("KeyEvent", listener);*/
}

void 
Label::handleMouseEvent(Events::MouseEventDataPtr &medp){
	printf("Label::%s() received mouse event at(%p)\n", __func__, medp.get());
}


