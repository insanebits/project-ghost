#ifndef UI_LABEL_H
#define UI_LABEL_H

#include "font_manager.h"
#include "block.h"
#include "event_types.h"

#include "IKeyListener.h"
#include "IMouseListener.h"
#include "ui_rect.h"
/*
#include "mouse_driver.h"
#include "key_driver.h"	// might be bad
*/

namespace UI{
	namespace Controls{

		class Label : public Block, public Events::IKeyListener, public Events::IMouseListener {

		public:
		protected:
			std::string m_FontName;
			int m_FontSize;
			std::string m_Text;
			Color m_FontColor;
		private:

		public:
			void			
			setFont(std::string fontName);

			void
			setFontSize(int fontSize);

			void
			setText(std::string text);
			
			std::string
			getText();

			void
			setFontColor(Color color);

			Label(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, UIRect maximumSize, int spacing);
			
			Label(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, int spacing);
		
			Label(WindowWPtr wpWindow, UIRect preferredSize, int spacing);

			bool 
			draw();
		
			void 
			handleKeyEvent(Events::KeyEventDataPtr &medp) override;

			void 
			handleMouseEvent(Events::MouseEventDataPtr &medp) override;

			void 
			init();

			void 
			handleEvent(Events::IEventProducerPtr &p, Events::IEventArgsPtr &a) override;


		protected:
		private:
		};
	}
}
#endif
