#include "text_block.h"
#include "ui_window.h"// might be dangerous 
using namespace UI::Controls;

void		
TextBlock::setFont(std::string fontName)
{
	m_FontName = fontName;			
}

void
TextBlock::setFontSize(int fontSize)
{
	m_FontSize = fontSize;			
}

void
TextBlock::setText(std::string text)
{
	m_Text = text;
}

void
TextBlock::setFontColor(Color color)
{
	m_FontColor = color;
}

TextBlock::TextBlock(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, UIRect maximumSize, int spacing):
	Block(wpWindow, preferredSize, minimumSize, maximumSize, spacing)
{			
	init();
}

TextBlock::TextBlock(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, int spacing):
	Block(wpWindow, preferredSize, minimumSize, spacing)
{
	init();			
}

bool 
TextBlock::draw()
{
	WindowPtr pWindow; // window handle

	Margin blockMargin = getMargin();
	Padding blockPadding = getPadding();
	Border blockBorder = getBorder();

	if(!(pWindow = m_wpWindow.lock()))
	{
		printf("Block::%s() could not lock window \n", __func__);
		return false;
	}

	int width = pWindow->getWindowWidth();
	int height = pWindow->getWindowHeight();

	int leftInset =  blockMargin.left + blockPadding.left + blockBorder.getThickness();
	int topInset =  blockMargin.top + blockPadding.top + blockBorder.getThickness();
	int rightInset = blockMargin.right + blockPadding.right + blockBorder.getThickness();
	int bottomInset = blockMargin.bottom + blockPadding.bottom + blockBorder.getThickness();

	//printf("Block insets %d %d %d %d \n", leftInset, topInset, rightInset, bottomInset);

	// drawing area that includes border, padding, margin
	int currentPointX = this->getPosition().coordX + leftInset;
	int currentPointY = this->getPosition().coordY + topInset;
	int drawToX = currentPointX + this->getPosition().width - rightInset;
	int drawToY = currentPointY + this->getPosition().height - bottomInset;


	//TODO add support for rounded corners

	Block::draw(); // will draw everything needed we only draw text

	//text rendering
	GLCoordinate start(height, width, currentPointX, currentPointY + m_FontSize);
	GLCoordinate finish(width, height, drawToX, drawToY);
	
	FontManagerPtr pFontManager = pWindow->getFontManager();
	if(!pFontManager)
	{
		printf("TextBlock::%s() could not get font manager instance\n", __func__);
		return false;
	}

	pFontManager->setCurrentFontColor(m_FontColor);
	pFontManager->setCurrentFont(m_FontName);
  pFontManager->setCurrentFontSize(m_FontSize);
	pFontManager->render_text(m_Text.c_str(), start, finish,  true, true);
	// preserve where our text was rendered
	//m_TextPosition = start;
	//m_TextPosition.width = finish.coordX - m_TextPosition.coordX;
	//m_TextPosition.height = finish.coorY - m_TextPosition.coordY; 
	return true;
}	

void 
TextBlock::handleKeyEvent(Events::KeyEventDataPtr &pKeyEventData){
	if(!pKeyEventData){
		printf("Not valid event data\n");
		return;
	}

	if(!m_Editable)
	{
		return; //nothing to do if not editable
	}

	if(!m_Focused)
	{
		return; // nothing to do if not focused on our field
	}

	if(pKeyEventData->isDown())
	{
		char character = pKeyEventData->getGData()->ascii;
		unsigned int charCode = static_cast<unsigned int>(character);
		switch(charCode)
		{
			case 0x08 : // backspace
			{
				if(!m_Text.empty())
				{
					m_Text = m_Text.substr(0, m_Text.length() - 1);
				}
			} break;

			default : 
			{
				//printf("Appending char\n");
				m_Text += character; // add character 
			} break;
		}// switch
	} // is down
}


void 
TextBlock::handleEvent(Events::IEventProducerPtr &p, Events::IEventArgsPtr &a) {
	if(p->getProducerType() == "MouseEvent")
	{
		Events::MouseEventDataPtr med = boost::dynamic_pointer_cast<Events::MouseEventData>(a);
		UIRect labelPosition = getPosition();

		int clickedX = med->getPositionX();
		int clickedY = med->getPositionY();
		WindowPtr pWindow;
		if(!(pWindow = getWindow().lock()))
		{
			printf("Label::%s() could not lock window", __func__);
			return; 
		}

		// TODO conversion should be in mouse driver or somewhere not so far
		pWindow->screenToClient(clickedX, clickedY, clickedX, clickedY); // converting to our window coordinates

		int labelStartX = labelPosition.coordX;
		int labelStartY = labelPosition.coordY;

		int labelEndX = labelStartX + labelPosition.width;
		int labelEndY = labelStartY + labelPosition.height;

		// only handle event if it's clicked on our block 
		if((clickedX >= labelStartX) && (clickedX <= labelEndX))
		{
			if((clickedY >= labelStartY) && (clickedY <= labelEndY))
			{
				m_Focused = true;
				handleMouseEvent(med);
				return; //handled so we can return
			}
		}

		m_Focused = false;
	}


		
	if(p->getProducerType() == "KeyEvent")
	{
		Events::KeyEventDataPtr ked = boost::dynamic_pointer_cast<Events::KeyEventData>(a);
		//passing data to internal method
		printf("TextBlock::%s() handling key event\n", __func__);
		this->handleKeyEvent(ked);
		printf("TextBlock::%s() key event handled\n", __func__);
		return;
	}
}

void
TextBlock::init()
{
	Events::IEventListenerPtr listener(this);
	WindowPtr pWindow;
	if(!(pWindow = m_wpWindow.lock()))
	{
		printf("TextBlock::%s() could not lock window\n", __func__);
		return;
	}	

	pWindow->getEventManager()->registerEventListener("MouseEvent", listener);
	pWindow->getEventManager()->registerEventListener("KeyEvent", listener);
}

void 
TextBlock::handleMouseEvent(Events::MouseEventDataPtr &medp){
	if(!m_Focused)
	{
		m_Focused = true;
	}
	
}

void 
TextBlock::toggleEditing()
{
	m_Editable = m_Editable ? false : true; 
}

void 
TextBlock::enableEditing()
{
	m_Editable = true;
}

bool
TextBlock::isEditable()
{
	return m_Editable;
}


