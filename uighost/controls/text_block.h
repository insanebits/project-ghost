#ifndef UI_CONTROLS_TEXTBLOCK_H
#define UI_CONTROLS_TEXTBLOCK_H
#include "font_manager.h"
#include "block.h"
#include "event_types.h"

#include "IKeyListener.h"
#include "IMouseListener.h"
#include "ui_rect.h"

namespace UI {
	namespace Controls{
		class TextBlock : public Block, public Events::IKeyListener, public Events::IMouseListener {

		public:
		protected:
			std::string m_FontName;
			int m_FontSize;
			std::string m_Text;
			Color m_FontColor;
			UIRect m_TextPosition; // hold where we have rendered our text
			bool m_Editable; // if field can be edited
			bool m_Focused; // if currently field is focused(can react to editing)
		private:

		public:
			void			
			setFont(std::string fontName);

			void
			setFontSize(int fontSize);

			void
			setText(std::string text);

			void
			setFontColor(Color color);

			TextBlock(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, UIRect maximumSize, int spacing);
			
			TextBlock(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, int spacing);

			bool 
			draw();
		
			void 
			handleKeyEvent(Events::KeyEventDataPtr &medp) override;

			void 
			handleMouseEvent(Events::MouseEventDataPtr &medp) override;

			void 
			init();

			void 
			handleEvent(Events::IEventProducerPtr &p, Events::IEventArgsPtr &a) override;

			void 
			toggleEditing();

			bool
			isEditable();

			void 
			enableEditing();

		protected:
		private:
		};
	}
}

#endif
