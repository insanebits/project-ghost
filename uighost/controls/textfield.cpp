#include "textfield.h"
#include "ui_window.h"// might be dangerous 
using namespace UI::Controls;

void		
TextField::setFont(std::string fontName)
{
	m_FontName = fontName;			
}

void
TextField::setFontSize(int fontSize)
{
	m_FontSize = fontSize;			
}

void
TextField::setText(std::string text)
{
	m_Text = text;
}

void
TextField::setFontColor(Color color)
{
	m_FontColor = color;
}

TextField::TextField(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, UIRect maximumSize, int spacing):
	Block(wpWindow, preferredSize, minimumSize, maximumSize, spacing)
{			
	init();
}

TextField::TextField(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, int spacing):
	Block(wpWindow, preferredSize, minimumSize, spacing)
{
	init();			
}

bool 
TextField::draw()
{
	WindowPtr pWindow; // window handle

	Margin blockMargin = getMargin();
	Padding blockPadding = getPadding();
	Border blockBorder = getBorder();

	if(!(pWindow = m_wpWindow.lock()))
	{
		printf("Block::%s() could not lock window \n", __func__);
		return false;
	}

	int width = pWindow->getWindowWidth();
	int height = pWindow->getWindowHeight();

	int leftInset =  blockMargin.left + blockPadding.left + blockBorder.getThickness();
	int topInset =  blockMargin.top + blockPadding.top + blockBorder.getThickness();
	int rightInset = blockMargin.right + blockPadding.right + blockBorder.getThickness();
	int bottomInset = blockMargin.bottom + blockPadding.bottom + blockBorder.getThickness();

	//printf("Block insets %d %d %d %d \n", leftInset, topInset, rightInset, bottomInset);

	// drawing area that includes border, padding, margin
	int currentPointX = this->getPosition().coordX + leftInset;
	int currentPointY = this->getPosition().coordY + topInset;
	int drawToX = currentPointX + this->getPosition().width - rightInset;
	int drawToY = currentPointY + this->getPosition().height - bottomInset;


	//TODO add support for rounded corners

	Block::draw(); // will draw everything needed we only draw text
	//glColor3f(&m_)
	//UI::drawBox()

	//text rendering
	GLCoordinate start(height, width, currentPointX, currentPointY + m_FontSize);
	GLCoordinate finish(width, height, drawToX, drawToY);
	
	FontManagerPtr pFontManager = pWindow->getFontManager();
	if(!pFontManager)
	{
		printf("Label::%s() could not get font manager instance\n", __func__);
		return false;
	}
	pFontManager->setCurrentFontColor(m_FontColor);
	pFontManager->setCurrentFont(m_FontName);
  pFontManager->setCurrentFontSize(m_FontSize);

	pFontManager->render_text(m_Text.c_str(), start, finish,  false, false);
	return true;
}	


void 
TextField::handleEvent(Events::IEventProducerPtr &p, Events::IEventArgsPtr &a) {
if(p->getProducerType() == "MouseEvent")
	{
		Events::MouseEventDataPtr med = boost::dynamic_pointer_cast<Events::MouseEventData>(a);
		UIRect labelPosition = getPosition();

		// only handle event if it's clicked on our block 
		if((med->getPositionX() >= labelPosition.coordX) && (med->getPositionX() <= (labelPosition.coordX + labelPosition.width)))
		{
			if((med->getPositionY() >= labelPosition.coordY) && (med->getPositionY() <= (labelPosition.coordY + labelPosition.height)))
			{
				handleMouseEvent(med);
				m_Focused = true;
			} else {
				m_Focused = false;
			}
		} else {
			m_Focused = false;
		}
		
		return;
	}

	if(p->getProducerType() == "KeyEvent")
	{
		Events::KeyEventDataPtr ked = boost::dynamic_pointer_cast<Events::KeyEventData>(a);
		//passing data to internal method
		if(m_Focused)
		{
			handleKeyEvent(ked);
		}
		return;
	}
	//KeyEventDataPtr ked = boost::dynamic_pointer_cast<KeyEventData>(a);
	// passing data to internal method
	//handleKeyEvent(ked);
}

void
TextField::init()
{
	Events::IEventListenerPtr listener(this);
	WindowPtr pWindow;
	if(!(pWindow = m_wpWindow.lock()))
	{
		printf("Label::%s() could not lock window\n", __func__);
		return;
	}	

	pWindow->getEventManager()->registerEventListener("MouseEvent", listener);
	pWindow->getEventManager()->registerEventListener("KeyEvent", listener);

	m_CursorState = false; // not visible by default
	m_Focused = false;
}

void 
TextField::handleMouseEvent(Events::MouseEventDataPtr &medp){
	printf("Label::%s() received mouse event at(%p)\n", __func__, medp.get());
	m_CursorState = true; // visible
	m_CursorStateChange = microsec_clock::local_time();
	
}


void 
TextField::handleKeyEvent(Events::KeyEventDataPtr &pKeyEventData){
	if(!pKeyEventData){
		printf("Not valid event data");
		return;
	}
	if(pKeyEventData->isDown())
	{
		char character = pKeyEventData->getGData()->ascii;
		printf("got char %d\n", character);
		if(character == static_cast<char>(0x08))// backspace
		{
			if(!m_Text.empty())
			{
				m_Text = m_Text.substr(0, m_Text.length() - 1);
			}
		} else {
			m_Text += character;
		}	
	}
}




