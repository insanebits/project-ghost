#ifndef TEXTFIELD_H
#define TEXTFIELD_H
#include "font_manager.h"
#include "block.h"
#include "event_types.h"

#include "IKeyListener.h"
#include "IMouseListener.h"
#include "ui_rect.h"

// for cursor blink
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#define BLINK_RATE 500 // in miliseconds


namespace UI{
	namespace Controls{

		using namespace boost::posix_time;

		class TextField : public Block, public Events::IKeyListener, public Events::IMouseListener
		{
		public:
		protected:
			std::string m_FontName;
			int m_FontSize;
			std::string m_Text;
			Color m_FontColor;
			//Color m_BackgroundColor;
			// cursor data
			bool m_CursorState; // when visible true
			ptime m_CursorStateChange; // when last time we blinked (set visible or not visible)
			UIRect m_CursorPosition;
			bool m_Focused; // if user focused on our field

		private:

		public:
			void			
			setFont(std::string fontName);

			void
			setFontSize(int fontSize);

			void
			setText(std::string text);

			void
			setFontColor(Color color);

			TextField(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, UIRect maximumSize, int spacing);
			
			TextField(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, int spacing);

			bool 
			draw();
		
			void 
			handleKeyEvent(Events::KeyEventDataPtr &medp) override;

			void 
			handleMouseEvent(Events::MouseEventDataPtr &medp) override;

			void 
			init();

			void 
			handleEvent(Events::IEventProducerPtr &p, Events::IEventArgsPtr &a) override;

			


		protected:
		private:
		};	

	}
}
#endif
