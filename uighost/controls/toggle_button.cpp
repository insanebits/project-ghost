#include "toggle_button.h"
using namespace UI::Controls;

ToggleButton::ToggleButton(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, UIRect maximumSize, int spacing):
	Label(wpWindow, preferredSize, minimumSize, maximumSize, spacing)
{		
}

ToggleButton::ToggleButton(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, int spacing):
	Label(wpWindow, preferredSize, minimumSize, spacing)
{			
}

void 
ToggleButton::handleMouseEvent(Events::MouseEventDataPtr &medp)
{
	if(m_CurrentState)
	{
		m_CurrentState = false; // toggling
		m_BackgroundColor = m_DisabledBackgroundColor;
		//m_Text = "OFF";
	} else {
		m_CurrentState = true; // toggling
		m_BackgroundColor = m_EnabledBackgroundColor;
		//m_Text = "ON";
	}
}

void
ToggleButton::setEnabledColor(Color c)
{
	m_EnabledBackgroundColor = c;
	if(m_CurrentState) // if is enabled we currently changed it's color so setting correct background
	{
			m_BackgroundColor = m_EnabledBackgroundColor;
	}
}

void
ToggleButton::setDisabledColor(Color c)
{
	m_DisabledBackgroundColor = c;
	if(!m_CurrentState) // if is enabled we currently changed it's color so setting correct background
	{
			m_BackgroundColor = m_DisabledBackgroundColor;
	}
}
