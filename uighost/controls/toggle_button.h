#ifndef TOGGLE_BUTTON_H
#define TOGGLE_BUTTON_H
#include "label.h"
namespace UI{
	namespace Controls{
		class ToggleButton : public Label
		{
		public:
			ToggleButton(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, UIRect maximumSize, int spacing);			
			ToggleButton(WindowWPtr wpWindow, UIRect preferredSize, UIRect minimumSize, int spacing);
			void 
			handleMouseEvent(Events::MouseEventDataPtr &medp); // where switching happens

			void
			setEnabledColor(Color c);

			void
			setDisabledColor(Color c);
		
		protected:
			Color m_EnabledBackgroundColor;
			Color m_DisabledBackgroundColor;
			bool m_CurrentState; // enabled or disabled		
		};
	}
}
#endif
