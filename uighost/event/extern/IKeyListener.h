#ifndef KEY_LISTENER_H
#define KEY_LISTENER_H

#include "event_types.h"
#include "key_driver.h"
#include <boost/shared_ptr.hpp>
#include <boost/cast.hpp>

namespace Events
{
	class IKeyListener;
	typedef boost::shared_ptr<IKeyListener> IKeyListenerPtr;
	
	class IKeyListener : public virtual IEventListener
	{
	public:
		IKeyListener(){}
		/*Implemented virtual method which will pass mouse data*/
		//virtual void 
		//handleEvent(IEventProducerPtr &p, IEventArgsPtr &a) = 0; //override {
//			KeyEventDataPtr ked = boost::dynamic_pointer_cast<KeyEventData>(a);
//			// passing data to internal method
//			handleKeyEvent(ked);
//		}
	protected:
		/*Method for derived classes to process event data*/
		virtual void 
		handleKeyEvent(KeyEventDataPtr &medp) = 0;
	private:
		
	};
}
#endif
