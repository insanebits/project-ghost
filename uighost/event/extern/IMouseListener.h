#ifndef IMOUSE_EVENT_LISTENER
#define IMOUSE_EVENT_LISTENER

#include "event_types.h"
#include "mouse_driver.h"
//#include "event_producer.h"
#include <map>
/*
class IEventListener
{
	public:
		virtual ~IEventListener()
		{}
		virtual void handleEvent(IEventProducerPtr &p, IEventArgsPtr &a) = 0;
		
};
*/
namespace Events{

	class IMouseListener;
	typedef boost::shared_ptr<IMouseListener> IMouseListenerPtr;

	class IMouseListener : public virtual IEventListener
	{
		public:
			//virtual void 
			//handleEvent(IEventProducerPtr &p, IEventArgsPtr &a) = 0;
			//{
			//	MouseEventDataPtr med = boost::dynamic_pointer_cast<MouseEventData>(a);
			//	// passing data to internal method
			//	handleMouseEvent(med);
			//}
			
			virtual void 
			handleMouseEvent(MouseEventDataPtr &medp) = 0;
	};
}
#endif
