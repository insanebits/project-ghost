#include "event_types.h"
#ifndef EVENT_CONTROLLER_H
#define EVENT_CONTROLLER_H
namespace Events{
	class EventManager;

	class EventListener: public IEventListener
	{
	public:
		EventListener();

		~EventListener();
		//handleEvent(IEventProducer&, IEventArgs&)
		void handleEvent(IEventProducerPtr &p, IEventArgsPtr &a);

	private:
		//typedef std::vector<boost::signals2::connection> ConnectionVec;
		//std::map<boost::shared_ptr<EventProducer>, ConnectionVec> m_allConnections;
		//std::set<boost::shared_ptr<EventProducer>> m_eventProducers;
	};
}
#endif
