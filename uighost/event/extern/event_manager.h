//#include "event_producer.h"
//#include "event_listener.h"
#ifndef EVENT_MANAGER_H
#define EVENT_MANAGER_H
#include <map>
#include "event_types.h"
#include <boost/shared_ptr.hpp>
namespace Events{

	class EventProducer;
	class EventListener;
       

	class EventManager;
	typedef boost::shared_ptr<EventManager> EventManagerPtr;


	class EventManager
	{
	public:
		// Notify listeners of all recent events
		void triggerAllQuenedEvents();
		~EventManager();
		
		EventManager(){
			printf("EventManager creating\n");
		}

		// Callback signature
		//typedef void EventNotificationFnSignature();
		//typedef boost::function<EventNotificationFnSignature> EventNotificationFn;

		//! Queue an event with the event manager
		void queueEvent(std::string eventType, IEventArgsPtr & eventArgs );
		
		/**
		 * string - type of event
		 * EventProducer - producer associated with event type to map listener with producer
		 */
		void registerEventType(std::string t, IEventProducerPtr &ep);
		
		/**
		 * string - type of event
		 * EventListener - listener to be mapped with producer
		 */
		void registerEventListener(std::string eventType, IEventListenerPtr & l);

		/**
		 * string - type of event
		 * EventListener - listener to be unregistered
		 */
		void unRegisterEventListener(std::string eventType, IEventListenerPtr & l);
	private:
		
		//typedef KeyValuePair<std::string/*key*/, IEventArgs * /*ptr to value*/> EventTypeArgsPair;
		//typedef std::vector<EventTypeArgsPair> EventArgsMap;
		//typedef std::vector<IEventProducer *> EventProducerVec;
		//typedef std::vector<IEventListener *> EventListenerVec;


		
		typedef std::vector<EventTypeArgsPairPtr> EventArgsMap;
		typedef std::vector<IEventProducerPtr> EventProducerVec;
		typedef std::vector<IEventListenerPtr> EventListenerVec; // weak pointer vector used 
		
		EventArgsMap mEventArgsMap;
		std::vector<std::string> mRegisteredEventTypes;
		std::map<std::string, IEventProducerPtr> mEventProducerMap;
		std::map<std::string, EventListenerVec> mEventListenerMap;

		// This mutex is used to ensure one-at-a-time access to the list of notifications
		//boost::recursive_mutex m_notificationProtection ;
	};
}

#endif
