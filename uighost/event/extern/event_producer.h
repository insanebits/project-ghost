#ifndef EVENT_PRODUCER_H
#define EVENT_PRODUCER_H
// Forward declarations
#include <string>
#include "event_types.h"

namespace Events
{
	class IEventProducer;
	class IEventManager;
	
	class EventProducer: public IEventProducer
	{
	public:
		EventProducer(const std::string & name/*producer name to know which is it*/, 
					  EventManagerWPtr & eventManager/*used to cause events*/);

		const std::string& getName();
		//void causeEvent( std::string s, IEventArgs &args);
		//void registerListener(IEventListener &l);
		void fireEvent(IEventArgsPtr &args);
		
		~EventProducer();

	private:
		std::string m_name;
		EventManagerWPtr m_EventManager;
	};
}
#endif
