#ifndef EVENT_TYPES_H
#define EVENT_TYPES_H

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp> 
namespace Events{

	/*--------------------------------------------------------------------------------*/
	// Forward definitions
	class EventProducer;
	class EventListener;
	class EventManager;

	template<typename T1,typename T2> class KeyValuePair;
	


	// interface definitions
	class IEventArgs;
	class IEventListener;
	class IEventProducer;

	/*--------------------------------------------------------------------------------*/
	// smart pointers    
	typedef boost::shared_ptr<IEventProducer> IEventProducerPtr;
	typedef boost::shared_ptr<IEventListener> IEventListenerPtr;
	typedef boost::shared_ptr<EventManager> EventManagerPtr;
	typedef boost::weak_ptr<EventManager> EventManagerWPtr;
	typedef boost::shared_ptr<IEventArgs> IEventArgsPtr;
	typedef boost::shared_ptr<KeyValuePair<std::string, IEventArgsPtr >> EventTypeArgsPairPtr;

	/*--------------------------------------------------------------------------------*/
	// Interfaces
	class IEventArgs
	{
		public:
			virtual ~IEventArgs(){}
	};

	class IEventListener
	{
		public:
			virtual ~IEventListener()
			{}
			IEventListener(){}
			virtual void handleEvent(IEventProducerPtr &p, IEventArgsPtr &a) = 0;
		
	};

	class IEventProducer
	{
		public:
			IEventProducer(std::string producerType):m_ProducerType(producerType){}
			virtual ~IEventProducer(){}
			// constructors cannot be virtual
			//virtual IEventProducer(EventManagerWPtr& wpEventManager);
			virtual void 
			fireEvent(IEventArgsPtr & args) = 0;

			virtual std::string 
			getProducerType() = 0;
	protected:
		std::string m_ProducerType;
	};

	/*class IMouseListener : public IEventListener
	{
	public:
		void 
		handleEvent(IEventProducerPtr &p, IEventArgsPtr &a){// TODO shouldn't be implemented here
		}
	};*/


	/*--------------------------------------------------------------------------------*/
	// other definitions
	template<typename T1, typename T2> class KeyValuePair
	{
		public:
			T1 key;
			T2 value;
			KeyValuePair()
			{}
			KeyValuePair(T1 k, T2 v): key(k), value(v)
			{}
			~KeyValuePair()
			{}
	};
	//TODO remove this class it's used only for test purposes
	class EventArgs: public IEventArgs
	{
		public:
			std::string name;
			int value;
			EventArgs(std::string n, int v): name(n), value(v)
			{}
			~EventArgs()
			{}
	};


}
#endif
