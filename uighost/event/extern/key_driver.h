#include "event_types.h"
#include "key_event_data.h"
//#include "event_producer.h"
#include <map>

#include "GHOST_Types.h"
#ifndef KEY_DRIVER_H
#define KEY_DRIVER_H
namespace Events{
    
  class EventProducer;// may be unused
	
	typedef boost::shared_ptr<KeyEventData> KeyEventDataPtr;

	class KeyDriver;
	typedef boost::shared_ptr<KeyDriver> KeyDriverPtr;

	/**
	 * Class to read events from keyboard and dispach them
	 */
	class KeyDriver:public IEventProducer{
	public:
		KeyDriver();
		/*required by IEventProducer*/
		KeyDriver(EventManagerWPtr &em, std::string producerType);

		std::string 
		getProducerType() override;

		/*required by IEventProducer*/
		void fireEvent(GHOST_TEventKeyData * keyData, bool down);
		void fireEvent(IEventArgsPtr &args);
		void fireEvent(KeyEventDataPtr & data);
		/* Method checks if currently clicked key changed it's state
		 * This is solution to GHOST problem when button down event is fired multiple times
		 */
		bool keyChanged(KeyEventDataPtr & data);
	protected:
		EventManagerWPtr wpEventManager;
		//hods which keys are currently clicked
		std::map<GHOST_TKey, bool>m_ActiveKeyMap;
	};
}
#endif
