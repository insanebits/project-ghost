#include "event_types.h"
#include "key_driver.h"
#ifndef KEY_LISTENER_H
#define KEY_LISTENER_H
namespace Events
{
	class KeyListener;
	typedef boost::shared_ptr<KeyListener> KeyListenerPtr;
	
	class KeyListener:public IEventListener/*no need for separate listener interface*/
	{
	public:
		KeyListener();
		/*Implemented virtual method which will pass mouse data*/
		void handleEvent(IEventProducerPtr &p, IEventArgsPtr &a);
	protected:
		/*Method for derived classes to process event data*/
		void handleKeyEvent(KeyEventDataPtr &medp);
	private:
		
	};
}
#endif
