#include "GHOST_Types.h" // required by MouseEventData(int x, int y, GHOST_TButtonMask button...
#include "event_types.h"
#include <boost/shared_ptr.hpp>
#include <map>


#ifndef MOUSE_DRIVER_H
#define MOUSE_DRIVER_H
namespace Events{
	class MouseEventData;
	typedef boost::shared_ptr<MouseEventData> MouseEventDataPtr;

	class MouseEventData : public IEventArgs
	{
	public:
		MouseEventData()
		{}
		MouseEventData(int x, int y, GHOST_TButtonMask button, int z): m_x(x), m_y(y), m_z(z), m_button(button)
		{}
		int getPositionX()
		{return m_x;}

		int getPositionY()
		{return m_y;}

		int getWheelRotation()
		{return m_z;}
		
		int getGButtonData()
		{return m_button;}

	protected:
		int m_x; // cursor x position
		int m_y; // cursor y position
		int m_z; // wheel dislacement
		GHOST_TButtonMask m_button; // triggered button
		// todo add ability to decode GHOST_keydata to our smart data
	private:
	};
	
	typedef boost::shared_ptr<MouseEventData> MouseEventDataPtr;
	
	class MouseDriver;
	typedef boost::shared_ptr<MouseDriver> MouseDriverPtr;
	/**
	 * Class to read events from keyboard will be controlled by main function
	 */
	class MouseDriver : public IEventProducer
	{
	public:
		//MouseDriver();
		MouseDriver(EventManagerWPtr & em, std::string producerType);

		std::string
		getProducerType() override;

		/*Interface method*/
		void fireEvent(MouseEventDataPtr & args);
		void fireEvent(IEventArgsPtr & args);
	protected:
		/*Internal implementation of fire event with correct data types*/
		//void fireButtonEvent(MouseEventDataPtr & keyData);
	private:
		EventManagerWPtr wpEventManager;
	};
}
#endif
