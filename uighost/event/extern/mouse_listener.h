#include "event_types.h"
#include "mouse_driver.h"// holds mouse data type
#ifndef MOUSE_LISTENER_H
#define MOUSE_LISTENER_H
namespace Events{
	class MouseListener;
	typedef boost::shared_ptr<MouseListener> MouseListenerPtr;

	class MouseListener:public IEventListener
	{	
	public:
		MouseListener();
		/*Implemented virtual method which will pass mouse data*/
		void handleEvent(IEventProducerPtr &p, IEventArgsPtr &a);
	protected:
		/*Method for derived classes to process event data*/
		void handleKeyEvent(MouseEventDataPtr &medp);
	private:
	
	};
}
#endif
