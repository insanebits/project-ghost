#ifndef UIGHOST_MACROS_H
#define UIGHOST_MACROS_H
// deprecated macro
// note that :: must be typedefed because it is taken as two params for DEPRECATED
#ifdef __GNUC__
#define DEPRECATED(func) func __attribute__ ((deprecated))
#elif defined(_MSC_VER)
#define DEPRECATED(func) __declspec(deprecated) func
#else
#pragma message("WARNING: You need to implement DEPRECATED for this compiler")
#define DEPRECATED(func) func
#endif

#endif// UIGHOST_MACROS_H

