#include "event_listener.h"
using namespace Events;
EventListener::EventListener()
{

}

EventListener::~EventListener()
{
	std::cout << "Destructing event listener" << std::endl;
}


void EventListener::handleEvent(IEventProducerPtr &p, IEventArgsPtr &a)
{
	boost::shared_ptr<EventArgs> args = boost::dynamic_pointer_cast<EventArgs>(a);
	std::cout<< "Caught event with name: " << args->name << " and value:  " << args->value << std::endl;
}
