#include "event_manager.h"
#include "event_types.h"
#include <boost/foreach.hpp>
using namespace::Events;

void EventManager::triggerAllQuenedEvents()
{ 
    // Now loop over the notification callbacks and call each one.
    // Since we're looping over the copy we just made, new events won't affect us.
	BOOST_FOREACH( EventTypeArgsPairPtr & args, mEventArgsMap)
    {
			printf("EventManager::%s() triggering event '%s' \n", __func__, args->key.c_str());
			printf("EventManager  listener count %d \n", mEventListenerMap.size());
    	std::map<std::string, EventListenerVec>::iterator foundListeners = mEventListenerMap.find(args->key);

    	if(foundListeners == mEventListenerMap.end())
			{
				printf("EventManager::%s() event has zero listeners \n", __func__);
				break;			
			}

			std::vector<IEventListenerPtr> listeners = foundListeners->second;
			
    	BOOST_FOREACH(IEventListenerPtr listener, listeners/* iterating through all event listeners */)
    	{
				printf("EventManager::%s() trying to trigger event of %s type\n", __func__, args->key.c_str());

    		IEventProducerPtr producer = mEventProducerMap.find(args->key)->second;
				if(!producer)
				{
					printf("EventManager::%s() producer was destroyed unexpectedly\n", __func__);
				}
    		
				if(listener)
				{	
					IEventListener * pEventListener = listener.get(); // for debugging
					listener->handleEvent(producer, args->value);
				} else {
					printf("EventManager::%s() listener was destroyed unexpectedly\n", __func__);
				}
    	}
        //mEventListenerMap[args.key].handleEvent(args.value);
    }
    //std::cout << "Before clear" << std::endl;
    mEventArgsMap.clear();
    //std::cout << "After clear" << std::endl;
}

void EventManager::queueEvent(std::string eventType, IEventArgsPtr & eventArgs )
{
    // One thread at a time.
    //boost::recursive_mutex::scoped_lock lock(  m_notificationProtection );
	EventTypeArgsPairPtr kvp(new KeyValuePair<std::string, IEventArgsPtr>(eventType, eventArgs));		
    mEventArgsMap.push_back(kvp);
}

void EventManager::registerEventType(std::string t, IEventProducerPtr & ep)
{
	printf("EventManager::%s() type '%s'\n", __func__, t.c_str());
	mEventProducerMap[t] = ep;
}

void EventManager::registerEventListener(std::string t, IEventListenerPtr & l)
{
	printf("EventManager::%s() type '%s'\n", __func__, t.c_str());
	mEventListenerMap[t].push_back(l);
}

EventManager::~EventManager()
{
	std::cout << "Listener map size before cleanup:" << mEventListenerMap.size() << std::endl;
	mEventListenerMap.clear();
	std::cout << "Listener map size after cleanup:" << mEventListenerMap.size() << std::endl;
	std::cout << "Producer map size before cleanup:" << mEventProducerMap.size() << std::endl;
	mEventProducerMap.clear();
	std::cout << "Producer map size after cleanup:" << mEventProducerMap.size() << std::endl;
	std::cout << "Destructing event manager" << std::endl;
}

void EventManager::unRegisterEventListener(std::string eventType, IEventListenerPtr & listener)
{
	std::cout << "ok"<< std::endl;
	std::map<std::string, EventListenerVec>::iterator it = mEventListenerMap.find(eventType);
	//if(it != mEventListenerMap.end())
	//{	
		for(std::vector<IEventListenerPtr>::iterator iter = it->second.begin(); iter != it->second.end(); iter++)
		//BOOST_FOREACH(IEventListenerPtr & listener, it->second)
		{
			if(listener == *iter)// found our listener
			{
				it->second.erase(iter);
			}
		}
	//}
}



