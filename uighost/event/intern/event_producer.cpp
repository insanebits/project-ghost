#include "event_producer.h"
#include "event_manager.h"
#include "event_types.h"

using namespace Events;

EventProducer::EventProducer(const std::string & name/*producer name to know which is it*/, EventManagerWPtr & eventManager/*used to cause events*/)
: m_name(name), m_EventManager(eventManager)
{
	//m_EventManager = pEventManager;
	//IEventProducerPtr epp(this);
	//m_EventManager->registerEventType("EventProducer", epp);// registering itself to be able to fire events	
}

const std::string&  EventProducer::getName()
{
    return m_name;
}

// Call this function to be notified of generic events
/*boost::signals2::connection subscribeToEvent( const std::string & eventType, const GenericEventHandlerFn & fn )
{
    if ( m_genericEventSignals[eventType] == NULL )
    {
        m_genericEventSignals[eventType].reset( new GenericEventSignal );
    }
    return m_genericEventSignals[eventType]->connect(fn);
}*/

/*void  EventProducer::causeEvent( std::string s, EventArgs &args)
{
    // Queue the event with the event manager
    m_pEventManager->QueueEvent(s, args); //< Must use boost::ref because signal is noncopyable.
}*/

/*void EventProducer::registerListener(IEventListener &l)
{
}*/

void EventProducer::fireEvent(IEventArgsPtr & args)
{
	if(EventManagerPtr emp = m_EventManager.lock())
	{
		emp->queueEvent("EventProducer", args);	
	} else {
		std::cout << "Could not get pointer to event producer" << std::endl;	
	}
}

EventProducer::~EventProducer()
{
	std::cout << "Destructing event producer" << std::endl;
}
