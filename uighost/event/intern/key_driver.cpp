#include "key_driver.h"
#include "event_manager.h"

using namespace Events;
/*
KeyDriver::KeyDriver()
{

}*/

KeyDriver::KeyDriver(EventManagerWPtr &em, std::string producerType) : wpEventManager(em), IEventProducer(producerType)
{
	if(wpEventManager.lock())
	{
		printf("KeyDriver::%s() valid event manager\n", __func__);
	}
}

void KeyDriver::fireEvent(KeyEventDataPtr & data)
{
	IEventArgsPtr eventArgs(boost::dynamic_pointer_cast<IEventArgs>(data));
	fireEvent(eventArgs);
}

void KeyDriver::fireEvent(GHOST_TEventKeyData * keyData, bool down)
{
	KeyEventDataPtr args(new KeyEventData(keyData, down));// ceating shared ptr to keyData
	IEventArgsPtr eventArgs(boost::dynamic_pointer_cast<IEventArgs>(args));
	fireEvent(eventArgs);	
}

void KeyDriver::fireEvent(IEventArgsPtr &args)
{
	if(EventManagerPtr em = wpEventManager.lock())
	{
		// this method is always used to quene events so key Change must be here
		KeyEventDataPtr keyData = boost::dynamic_pointer_cast<KeyEventData>(args);
		if(keyChanged(keyData))
		{
			// if changed we need to keep track of it
			if(keyData->isDown())
			{
				m_ActiveKeyMap[keyData->getGData()->key] = true;
			} else {
				m_ActiveKeyMap[keyData->getGData()->key] = false;
			}
			em->queueEvent(m_ProducerType, args);
		}
	} else {
		printf("KeyDriver::%s() invalid event manager\n", __func__);
	}
}

bool KeyDriver::keyChanged(KeyEventDataPtr & data)
{
	GHOST_TEventKeyData * ghostKeyData = data->getGData();
	if(ghostKeyData)
	{
		std::map<GHOST_TKey, bool>::iterator mapIt = m_ActiveKeyMap.find(ghostKeyData->key);
		if(mapIt != m_ActiveKeyMap.end())
		{
			// if key is down and currently got same down event nothing changed
			if((mapIt->second == true)&&(data->isDown()))
			{
				return false;
			}
		}
	}
	return true;
}

std::string
KeyDriver::getProducerType()
{
	return m_ProducerType;
}
