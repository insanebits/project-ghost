#include "key_listener.h"

using namespace Events;

/*
		void handleEvent(IEventProducerPtr &p IEventArgsPtr &a);
		void handleMouseEvent(IMouseEventDataPtr &medp);
*/
KeyListener::KeyListener()
{

}

void KeyListener::handleEvent(IEventProducerPtr &p, IEventArgsPtr &a)
{
	KeyEventDataPtr med = boost::dynamic_pointer_cast<KeyEventData>(a);
	// passing data to internal method
	handleKeyEvent(med);
}

void KeyListener::handleKeyEvent(KeyEventDataPtr &medp)
{
	std::cout << "Got key event: key position" << (medp->isDown()? " Down " : " Up ")  << " button clicked: "<< medp->getData()->ascii << std::endl; 
}
