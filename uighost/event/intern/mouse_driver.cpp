#include "mouse_driver.h"
#include "event_manager.h"

using namespace Events;

/*MouseDriver::MouseDriver()
{
}*/

/*
void MouseDriver::handleButtonDown(GHOST_TEventButtonData *keyData)
{
	std::cout << "Caught left mouse key down " << keyData->button << std::endl;
}

void MouseDriver::handleButtonUp(GHOST_TEventButtonData *keyData)
{
	std::cout << "Caught left mouse key up " << keyData->button << std::endl;
}

void MouseDriver::handleWheel(GHOST_TEventWheelData *keyData)
{
	std::cout << "Caught mouse scroll " << keyData->z << std::endl;
}

void MouseDriver::handleCursorMove(GHOST_TEventCursorData * cursorData)
{
	std::cout << "Caught cursor movement " << cursorData->x << " " << cursorData->y << std::endl;
}*/

MouseDriver::MouseDriver(EventManagerWPtr & em, std::string producerType):wpEventManager(em), IEventProducer(producerType)
{
}

void MouseDriver::fireEvent(IEventArgsPtr & args)
{
	if(EventManagerPtr em = wpEventManager.lock())
	{
		em->queueEvent(m_ProducerType, args);
	}
}

void MouseDriver::fireEvent(MouseEventDataPtr & args)
{
	//MouseEventDataPtr args(keyData);// ceating shared ptr to keyData
	IEventArgsPtr eventArgs = boost::dynamic_pointer_cast<IEventArgs>(args);	
	fireEvent(eventArgs);
}


std::string
MouseDriver::getProducerType()
{
	return m_ProducerType;
}
