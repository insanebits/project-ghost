#include "mouse_listener.h"
using namespace Events;

MouseListener::MouseListener()
{

}

void MouseListener::handleEvent(IEventProducerPtr &p, IEventArgsPtr &a)
{
	MouseEventDataPtr med = boost::dynamic_pointer_cast<MouseEventData>(a);
	// passing data to internal method
	handleKeyEvent(med);
}

void MouseListener::handleKeyEvent(MouseEventDataPtr &medp)
{
	std::cout << "Got mouse event: key position" << std::endl;
}
