#requirements for this library
#find_package(OpenGL REQUIRED)
##find_package(Boost COMPONENTS system filesystem REQUIRED)
#find_package(Freetype REQUIRED)
find_package(GLEW REQUIRED)

include_directories(${Boost_INCLUDE_DIR})
link_directories(${Boost_LIBRARY_DIR})
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
#other definitions
#add_definitions(-DFT_FREETYPE_H=/usr/include/freetype2/freetype/freetype.h)
#add_definitions(-DCMAKE_CXX_COMPILER=g++)
#add_definitions(-DCMAKE_CC_COMPILER=gcc)
#add_definitions(-DLDLIBS=-lm -lglut -lGLEW -lGL -lfreetype -lboost_system -lboost_filesystem)

SET(SRC 
	font_manager.cpp
	shader_utils.cpp
	../ui_drawing.cpp
)

SET(INC
	../
)

include_directories(${INC})

add_library(fontlib SHARED ${SRC})
message(STATUS "GLEW:"	${GLEW_LIBRARIES})
target_link_libraries(fontlib 
	boost_system
	freetype
	glut
	GL
	GLEW
)

