/* 
 * File:   font_api.h
 * Author: insane
 *
 * Created on May 16, 2013, 11:55 AM
 */

#ifndef FONT_API_H
#define	FONT_API_H

#include <map>  
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/smart_ptr/weak_ptr.hpp>


//#include <GL/glew.h>
#include <GL/gl.h>

//#if defined(__APPLE__)
//    #include <Glut/glut.h>
//#elif defined(_WIN32) || defined(_WIN64)
//    #include <GLUT/glut.h>
//#else
//    #include <GL/glut.h>
		
//#endif

#include <ft2build.h>
#include FT_FREETYPE_H

#include "font_types.h"
#include "shader_utils.h"
#include "ui_drawing.h"
//#include "ui_types.h" // for color
#include "font_manager.h"

typedef struct {
    float x, y, z;    // position
    float s, t;       // texture
    float r, g, b, a; // color
} vertex_t;

#endif	/* FONT_API_H */

