#ifndef "FONT_CHAR_H"
#define "FONT_CHAR_H"
/*G stands for ghost*/
namespace GFont
{
	class GChar{
	public:
		GChar(char ascii) : m_Ascii(ascii) {}
		
		int
		getWidth() { return m_Width;}

		void
		setWidth(int width) { m_Width = width;}

		int 
		getHeight() { return m_Height;}

		void
		setHeight(int height) { m_Height = height;}
		
	protected:
		char m_Acii; // char representation
		int m_Width;  // how much space char takes set by font
		int m_Height; // may be required if typing vertically
	}

}

#endif;
