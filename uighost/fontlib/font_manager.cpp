#include "font_manager.h"
static FontFacePtr FontFacePtrNull; // to initialize once

FontManager::FontManager(std::vector<std::string> fontFileList)
{

	// initializing Freetype and uniforms
	if(initResources() != 1)
	{
		fprintf(stderr, "Could not init FontManager resources\n");
	}

	for(unsigned int j = 0; j < fontFileList.size(); j++){
		std::vector<std::string> vct;
		boost::split(vct, fontFileList[j], boost::is_any_of("/"));
		std::string filename;	
	
		if(vct.size() > 0)
		{
			filename = vct.back();
			std::vector<std::string> filenameParts;					
			boost::split(filenameParts, filename, boost::is_any_of("."));
			if(filenameParts.size() > 1){//there is file name and extension
				filenameParts.pop_back();//remove extension
				filename = boost::join(filenameParts, ".");
			} else {
				continue;// skip adding to list					
			}
		} else {// this is only filename
			filename = vct.back();
			std::vector<std::string> filenameParts;					
			boost::split(filenameParts, filename, boost::is_any_of("."));
			if(filenameParts.size() > 1){//there is file name and extension
				filenameParts.pop_back();//remove extension
				filename = boost::join(filenameParts, ".");
			} else {
				continue;// skip adding to list					
			}
		}
    // convert filename to lowercase
		boost::algorithm::to_lower(filename);
		//adding file to filelist
		//FontFacePtr fontFace(new FontFace());
		FontFacePtr fontFace(new FontFace());
		printf("Font face loading from(%s)\n", fontFileList[j].c_str());
		if (FT_New_Face(m_FreetypeLibrary, fontFileList[j].c_str(), 0, fontFace.get()) == 0){ // try to load font
			printf("Font loaded successfully\n");
			//FontFacePtr fontFace(&ff);
			m_FontMap.insert(m_FontMap.begin(), std::pair<std::string, FontFacePtr>(filename, fontFace)); 
		} else {
			fprintf(stderr, "Could not load font from (%s)\n", fontFileList[j].c_str());
		}
	}	// end for
}


void 
FontManager::render_text(const char *text, GLCoordinate & current, GLCoordinate finish,  bool wrap, bool overflow) {
		current.setWindowBounds(m_WindowWidth, m_WindowHeight);
		finish.setWindowBounds(m_WindowWidth, m_WindowHeight);
		beforeRendering();
		std::vector<std::string> lines = preprocessText(text, current, finish, wrap);

    std::vector<std::string>::iterator lineIterator;
    std::string::iterator wordIterator;
    
		// setting current bounds for correct representation



		const char *p;
		FontFacePtr face(this->getCurrentFont());

		
		FT_GlyphSlot g = (*face)->glyph;

		if(g == nullptr)// glypth was not loaded
		{
			return;		
		}

		GLCoordinate _start = current; // copy start for wrapping

		/* Create a texture that will be used to hold one "glyph" */
		GLuint tex;

		glActiveTexture(GL_TEXTURE0);
		glGenTextures(1, &tex);
		glBindTexture(GL_TEXTURE_2D, tex);
		glUniform1i(m_UniformTexture, 0); // bind sampler to texture

		/* We require 1 byte alignment when uploading texture data */
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		/* Clamping to edges is important to prevent artifacts when scaling */
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		/* Linear filtering usually looks best for text */
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		/* Set up the VBO for our vertex data */
		glEnableVertexAttribArray(m_AttributeCoord);
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
		int totalX = 0; // for testing

		glVertexAttribPointer(m_AttributeCoord, 4, GL_FLOAT, GL_FALSE, 0, 0);
		/* Loop through all lines */
    for(lineIterator = lines.begin(); lineIterator != lines.end(); lineIterator++)
    {
      /* Loop through all characters in line */
      for(wordIterator = (*lineIterator).begin(); wordIterator != (*lineIterator).end(); wordIterator++)
      {
        /* Try to load and render the character */
        if (FT_Load_Char((*face), *wordIterator, FT_LOAD_RENDER))
          continue;
        /* Upload the "bitmap", which contains an 8-bit grayscale image, as an alpha texture */
        glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, g->bitmap.width, g->bitmap.rows, 0, GL_ALPHA,
         GL_UNSIGNED_BYTE, g->bitmap.buffer);

        float x2 = current.gx + current.gsx * g->bitmap_left; 
        float y2 = - current.gy - current.gsy * g->bitmap_top;	
        float w = g->bitmap.width * current.gsx;
        float h = g->bitmap.rows * current.gsy;

        point box[4] = {
          {x2,     -y2,     0, 0},
          {x2 + w, -y2,     1, 0},
          {x2,     -y2 - h, 0, 1},
          {x2 + w, -y2 - h, 1, 1},
        };

        /* Draw the character on the screen */
        glBufferData(GL_ARRAY_BUFFER, sizeof box, box, GL_DYNAMIC_DRAW);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        /* Advance the cursor to the current of the next character */
        //x += (g->advance.x >> 6) * sx;
        //y += (g->advance.y >> 6) * sy;
        current.addX(g->advance.x >> 6);
				totalX += ( g->advance.x >> 6 );
        current.addY(g->advance.y >> 6);
      }
      //printf("FontManager::%s() line width is %d \n", __func__, current.sx - _start.sx);
       _start.addY(m_CurrentFontSize); // drop by current font size
       current = _start;
   }
		//printf("Total rendered text length %d \n", totalX);
		glDisableVertexAttribArray(m_AttributeCoord);
		glDeleteTextures(1, &tex);

		afterRendering();
	}

FontFacePtr
FontManager::getCurrentFont(){	
		std::string currentFace = m_CurrentFontFace; // preserve case of original.
		boost::algorithm::to_lower(currentFace);

		FontMap::iterator it = m_FontMap.find(currentFace);
		FontFacePtr fontFace = it->second;	
		if(it != m_FontMap.end())
		{
			return it->second;
		} 

		fprintf(stderr, "Could not get current font\n");
		return FontFacePtrNull;
	}
/*
 * Method to preprocess text adding wrapping if needed
 * If wrapping is enabled words will be wrapped by word
 * Otherwise it will be wrapped cut of
 */
std::vector<std::string>
FontManager::preprocessText(const char * text, GLCoordinate start, GLCoordinate end, bool wrap)
{
	std::vector<std::string> wrapped; // vector of lines
	std::vector<std::string> words; // word list
	std::string givenText(text); // text to wrap
	std::vector<std::string>::iterator wordIterator; // to iterate word list
	int lineWidth = 0;
	FontFacePtr face(this->getCurrentFont());
	FT_GlyphSlot g = (*face)->glyph;
  std::string line;
	int spaceWidth = 0; // how much space takes 'space char'
	char * spaceChar = new char[1]; // hex of space symbol
	int maxLineWidth = end.sx - start.sx; // screen coordinate end minus screen coordinate start
	spaceChar[0] = 0x20;

  // split givenString into words list
	boost::split(words, givenText, boost::is_any_of(" "), boost::token_compress_on);
	
	if(FT_Load_Char((*face), *spaceChar, FT_LOAD_RENDER))
	{
		
	}
spaceWidth = (g->advance.x >> 6);

	//printf("Space width %d \n", spaceWidth);

	for(wordIterator = words.begin(); wordIterator != words.end(); wordIterator++)
	{
    int wordWidth = 0; // current word width
		std::string::iterator charIterator;

		for(charIterator = (*wordIterator).begin(); charIterator != (*wordIterator).end(); charIterator++)
		{
			if (FT_Load_Char((*face), (*charIterator), FT_LOAD_RENDER))
			{
				printf("FontManager::%s() Could not load char('%d')\n", __func__, *charIterator);
				continue;// could not load char			
			}

      wordWidth += (g->advance.x >> 6); // advance x * 64 because glyph metrics is in 1/64 of pixel
		}

		// deciding if we need to put current word to a new line
		int currentLineWidth = lineWidth + wordWidth + spaceWidth; // current line width plus word size plus space between current line and word
		if(currentLineWidth <= maxLineWidth) // may be equal means it can fit perfectly
		{
			lineWidth += wordWidth;
      if(!line.empty())
			{
        line.append(" "); // if line is not empty we will put space be
				// and add to string length of added space
				lineWidth += spaceWidth; 
      }
			line.append(*wordIterator);
		} else {
			//printf("Line width is %d \n", lineWidth);
			lineWidth = wordWidth; // current line width is wrapped word width
			wrapped.push_back(line); // adding current line to wrapped line vector
			line = (*wordIterator);// put word in a new line
		}

	}
  wrapped.push_back(line); // adding last line which wasn't added because was not full

	return wrapped;
}

int
FontManager::setCurrentFont(std::string font){
		boost::algorithm::to_lower(font);
			
		FontMap::iterator it = m_FontMap.find(font);
		
		if(it != m_FontMap.end())
		{
			m_CurrentFontFace = font;
			return 1;
		}
		printf("FontManager::%s() Font(%s) does not exist in font list", __func__, font.c_str());
		return 0;
	} 

int
FontManager::setCurrentFontSize(int fontSize){
	m_CurrentFontSize = fontSize;
	return 1; // success
}

void
FontManager::setCurrentFontColor(UI::Color color)
	{
		m_CurrentFontColor = color;
	}

void
FontManager::beforeRendering(){
		// Without this there will be blocks of colors
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glUseProgram(m_ShaderProgram); // first thing before setting uniforms
		
		// setup font color
		glUniform4fv(m_UniformColor, 1, decodeRGBA(&m_CurrentFontColor));
		//glUniform4fv(m_UniformColor, 1, red);
		// setup font size
		FT_Set_Pixel_Sizes((*getCurrentFont()), 0, m_CurrentFontSize);
	}

void 
FontManager::afterRendering(){
		// unset shader program after finished rendering
		glUseProgram(0);	
}

int 
FontManager::initResources(){
		//init freetype
		if (FT_Init_FreeType(&m_FreetypeLibrary)) {
			fprintf(stderr, "Could not init freetype library\n");
			return 0;
		} else {
			printf("Successfully loaded Freetype\n");		
		}

		//init shader
		m_ShaderProgram = create_program("text.v.glsl", "text.f.glsl");
		if(m_ShaderProgram == 0){
			return 0;
		}
		// init uniforms and attributes
		m_AttributeCoord = get_attrib(m_ShaderProgram, "coord");
		m_UniformTexture = get_uniform(m_ShaderProgram, "tex");
		m_UniformColor = get_uniform(m_ShaderProgram, "color");

		if(m_AttributeCoord == -1 || m_UniformTexture == -1 || m_UniformColor == -1){
      printf("FontManager::%s one of uniforms is not valid\n", __func__);
			return 0;
		}

		// Create the vertex buffer object
		glGenBuffers(1, &m_VBO);

		return 1;
}

void 
FontManager::setWindowBounds(int width, int height){
	m_WindowWidth = width;
	m_WindowHeight = height;
}
