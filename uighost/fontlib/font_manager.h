#ifndef FONT_MANAGER_H
#define FONT_MANAGER_H
#include "font_api.h"
#include "shader_utils.h"
#include "ui_types.h"

class FontManager;
typedef boost::shared_ptr<FontManager>FontManagerPtr;

typedef FT_Face FontFace;
typedef boost::shared_ptr<FontFace> FontFacePtr;

// holds all fonts
typedef std::map<std::string, FontFacePtr> FontMap;

class FontManager{
private:
	FontMap m_FontMap;
	FT_Library m_FreetypeLibrary;

	std::string m_CurrentFontFace;
	int m_CurrentFontSize;
	UI::Color m_CurrentFontColor;
  
  int m_WindowWidth;
  int m_WindowHeight;

	int initResources();

public:

		// uniforms	
	GLuint m_ShaderProgram;
	GLuint m_VBO;// vertex buffer object
	GLint m_AttributeCoord;
	GLint m_UniformTexture;
	GLint m_UniformColor;
	FontManager(std::vector<std::string> fontFileList);

	/**
	 * Render text using the currently loaded font and currently set font size.
	 * Rendering starts at coordinates (x, y), z is always 0.
	 * The pixel coordinates that the FreeType2 library uses are scaled by (sx, sy).
	 */
	void 
	render_text(const char *text, GLCoordinate & current, GLCoordinate finish,  bool wrap, bool overflow);

	FontFacePtr
	getCurrentFont();

	int
	setCurrentFont(std::string font);

	std::vector<std::string> 
	preprocessText(const char * text, GLCoordinate start, GLCoordinate end, bool wrap);

	int
	setCurrentFontSize(int fontSize);

	void
	setCurrentFontColor(UI::Color color);
        
  /**
   * Method to set screen bounds 
   * Window bounds are needed to calculate text position on screen
   * It should be changed when window is resized
   * @param width - screen width
   * @param height - screen height 
   */
  void 
  setWindowBounds(int width, int height);
        
private:
        /**
         * Method doing everything that is needed before rendering
         */
	void
	beforeRendering();
                
	void 
	afterRendering();


};//end of FontManager class

#endif
