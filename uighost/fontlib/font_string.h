#ifndef FONT_STRING_H
#define FONT_STRING_H

namespace GFont{
	class GString
	{
	public:
		typedef boost::shared_ptr<GChar> GCharPtr;

		GString(std::string str)
		{
			for(std::string::iterator it = str.begin(); it != str.end(); it++)
			{
				GCharPtr pCharacter(new GChar(*it));
				m_GCharList.push_back(pCharacter);
			}
		}

		std::string
		toString()
		{
			for(GCharList::iterator it = m_GCharList.begin(); m_GCharList != str.end(); it++)
			{
				GCharPtr pCharacter(new GChar(*it));
				m_CharList.push_back(pCharacter);
			}
		}
	protected:
		typedef std::list<GCharPtr> GCharList;
		GCharList m_GCharList;
	};
}

#endif
