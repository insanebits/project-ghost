#ifndef FONT_TYPES_H
#define	FONT_TYPES_H
// new line for current system
#ifdef _WIN32
    #define NEWLINE "\r\n"
#elif defined macintosh // OS 9
    #define NEWLINE "\r"
#else
    #define NEWLINE "\n" // Mac OS X uses \n
#endif


struct GLCoordinate {
	int sx; // x coordinate by screen
	int sy; // y coordinate by screen

  int sw; // screen width(more correctly window width)
	int sh; // screen height(more correctly window width)

	GLfloat gsx; // gl x pixel size
	GLfloat gsy; // gl y pixel size basicaly 2 / screenheigth

  GLfloat gx; // x coordinate in OpenGL
	GLfloat gy; // y coordinate in OpenGL
	 
	GLCoordinate(
		int width, 
		int height,
		int x,
		int y
	): sx(x), sy(y), sw(width), sh(height)
	{
		gsx = 2.0 / width;
		gsy = 2.0 / height;

		gx = -1.0 + gsx * sx;
		gy =  1.0 - gsy * sy;
	}
	/*
		results (sx,sy) => (sx+x,sy) 	
	*/
	void addX(int x){
		sx += x;
		gx += gsx * x; 
	}
	/*
		results (sx,sy) => (sx,sy+y) 	
	*/
	void addY(int y){
		sy += y;
		gy -= gsy * y; 	
	}

	void setWindowBounds(int width, int height)
	{
		sw = width;
		sh = height;

		gsx = 2.0 / width;
		gsy = 2.0 / height;

		gx = -1.0 + gsx * sx;
		gy =  1.0 - gsy * sy;
	}
};

//nullptr workaround
/*

const                        // this is a const object...
class {
public:
  template<class T>          // convertible to any type
    operator T*() const      // of null non-member
    { return 0; }            // pointer...
  template<class C, class T> // or any type of null
    operator T C::*() const  // member pointer...
    { return 0; }
private:
  void operator&() const;    // whose address can't be taken
} nullptr = {};              // and whose name is nullptr
**/

struct point {
	GLfloat x;
	GLfloat y;
	GLfloat s;
	GLfloat t;
};


#endif
