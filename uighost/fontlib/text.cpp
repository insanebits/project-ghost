#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glew.h>
#include <GL/glut.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <iomanip> 

#include <ft2build.h>
#include FT_FREETYPE_H

#include "shader_utils.h"

#include <map>
#include <vector>

#include <boost/circular_buffer.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/smart_ptr/weak_ptr.hpp>
#include <boost/filesystem.hpp>
#include "font_api.h"
#include "font_manager.h"
FontManager * fm;



void renderPrimitive()
{
	//glUseProgram(0); // disable text shader while drawing other things
  int width =  glutGet(GLUT_WINDOW_WIDTH);
  int height = glutGet(GLUT_WINDOW_HEIGHT);

	float wpx = 2.0/width;
  float hpx = 2.0/height;
	
	/*GLuint vbo1;
	GLuint vbo2;

	glGenBuffers(1, &vbo1);
	//glGenBuffers(1, &vbo2);

	glBindBuffer(GL_ARRAY_BUFFER, vbo1);

			// Each set of 4 vertices form a quad
      glColor3f(0.1, 0.2, 0.3);
			float data[] = {
				-1.0f,  1.0f,  
				 1.0f,  1.0f, 
				 1.0f,  1.0 - hpx*48, 
				-1.0f,  1.0 - hpx*48};

	glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, vbo1);
	glEnableClientState(GL_VERTEX_ARRAY);
	glDrawArrays(GL_TRIANGLES, 0, sizeof(data) / sizeof(float) / 3);*/

}

/**
 * Render text using the currently loaded font and currently set font size.
 * Rendering starts at coordinates (x, y), z is always 0.
 * The pixel coordinates that the FreeType2 library uses are scaled by (sx, sy).
 */

void display() {
	int width = glutGet(GLUT_WINDOW_WIDTH);
	int height = glutGet(GLUT_WINDOW_HEIGHT);
  
  //printf("%d", glutGet(GLUT_WINDOW_HEIGHT));
  
	//glUseProgram(fm->m_ShaderProgram);

	/* White background */
	glClearColor(1, 1, 1, 1);
	glClear(GL_COLOR_BUFFER_BIT);

  renderPrimitive();

	/* Enable blending, necessary for our alpha texture */
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	GLfloat black[4] = { 0, 0, 0, 1 };
	GLfloat red[4] = { 1, 0, 0, 1 };
	GLfloat transparent_green[4] = { 0, 1, 0, 0.5 };

	//glUniform4fv(fm->m_UniformColor, 1, red);
	fm->setCurrentFontColor(red);

	GLCoordinate start(width, height, 0, 48);
	GLCoordinate finish(width, height, width - 250, height - 200); 

	GLCoordinate coord2 = start; 

	coord2.addY(52);// font height + space
	fm->setCurrentFont("freesans");
	fm->setCurrentFontSize(48);
	fm->render_text("The Quick Brown Fox Jumps Over The Lazy Dog The Quick Brown Fox Jumps Over The Lazy Dog", start, finish, true, false);
	//fm->setCurrentFontColor(black);
	//fm->setCurrentFont("alexb");
	//fm->setCurrentFontSize(25);
	//fm->render_text("The Quick Brown Fox Jumps Over The Lazy Dog", coord, finish, true);
	glutSwapBuffers();
}

std::vector<std::string>
getFontList()
{
	namespace fs = boost::filesystem;
	fs::path someDir("./fonts");
	fs::directory_iterator end_iter;

	typedef std::vector<fs::path> result_set_t;
	result_set_t result_set;

	if ( fs::exists(someDir) && fs::is_directory(someDir))
	{
		for( fs::directory_iterator dir_iter(someDir) ; dir_iter != end_iter ; ++dir_iter)
		{
		  if (fs::is_regular_file(dir_iter->status()) )
		  {
		    result_set.push_back(*dir_iter);
			}
		}
	}

	std::vector<std::string> files;
	for(result_set_t::iterator it = result_set.begin(); it != result_set.end(); it++)
	{
		files.push_back(it->string());
	}	

	return files;
}


int main(int argc, char *argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB);
	glutInitWindowSize(640, 480);
	glutCreateWindow("Basic Text");

	GLenum glew_status = glewInit();

	if (GLEW_OK != glew_status) {
		fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
		return 1;
	}

	if (!GLEW_VERSION_2_0) {
		fprintf(stderr, "No support for OpenGL 2.0 found\n");
		return 1;
	}

	std::vector<std::string> files = getFontList();

	fm = new FontManager(files);
	fm->setCurrentFont("freesans");
	fm->setCurrentFontSize(48);

	glutDisplayFunc(display);
	glutMainLoop();
	return 0;
}
