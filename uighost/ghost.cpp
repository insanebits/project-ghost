#include "ui_api.h"

using namespace UI;

Application::Application(GHOST_ISystem *system)
	: m_system(system), m_timer(0), m_cursor(GHOST_kStandardCursorFirstCursor),
	m_exitRequested(false), stereo(false)
{
	using namespace Events;

	fApp = this;
	IEventProducerPtr pEventProducer;
	IEventListenerPtr pEventListener;
	
	//Event manager initialization
	wpEventManager = pEventManager; // weak pointer can be assigned
	// key driver initialization and it's event type registration
	//keyDriver.reset(new Events::KeyDriver(wpEventManager));

	pWindowManager = new UI::WindowManager(m_system);
	// needs to be created before GLEW is initialized
	customWindow = pWindowManager->createWindow("GHOST OpenGL GUI demo ", 0, 0, 640, 480).get();
}


Application::~Application(void)
{
	// Dispose windows
//if (m_system->validWindow(customWindow->getGWindow().get())) {
//		m_system->disposeWindow(customWindow->getGWindow().get());
		//m_system->disposeWindow(customWindow->get());
		//delete customWindow; 
		m_system->disposeWindow(customWindow->getGWindow().get());
//	}
}

void 
Application::initResources()
{
	if(!m_system)
	{
		printf("GHOST system does not exit \n");		
	}
	std::vector<std::string> files;
	files.push_back("/home/insane/Desktop/opengl/ghost_separated/build/bin/fonts/Vera.ttf");
	files.push_back("fonts/ObelixPro.ttf");
	files.push_back("fonts/VeraMono.ttf");
	files.push_back("fonts/VeraMoBd.ttf");

	pFontManager.reset(new FontManager(files));
	pWindowManager->setFontManager(pFontManager);
}

void
Application::createLayout()
{

	WindowPtr pCustomWindow = pWindowManager->getWindow(customWindow->getGWindow().get()); // hacky!!
	WindowWPtr wpCustomWindow = pCustomWindow;

	UI::Layout::ILayout * pLayout;
	UI::Layout::BorderLayout * pBorderLayout;


	IBlockPtr mainBlock = customWindow->getBlock();

	pLayout = new UI::Layout::BorderLayout(mainBlock.get());
	pBorderLayout = (UI::Layout::BorderLayout *) pLayout; // not sure if works;
	//mainBlock->getLayout()->setPositioning(Spacing::VERTICAL);
	mainBlock->setLayout(pBorderLayout);
	mainBlock->setBackgroundColor(Color(Colors::LightGreen));
	mainBlock->setPadding(Padding(10));
	mainBlock->setMargin(Margin(0));
	mainBlock->setBorder(Border(1, Color(Colors::Grey)));

	printf("Window size %dx%d\n", pCustomWindow->getWindowWidth(), pCustomWindow->getWindowHeight());

	
	UI::Controls::Label * labelBlock;

/*-------------------------------------------------center block-----------------------------------------------------------------*/

	UI::Controls::TextBlock * pTextBlock;
	pTextBlock = new UI::Controls::TextBlock(wpCustomWindow, UIRect(150, 75), UIRect(50, 50), Spacing::HORIZONTAL);
  Color background(Colors::White);
	pTextBlock->setBackgroundColor(Color(Colors::Yellow));
	pTextBlock->setText("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");

	pTextBlock->setFont("vera");
	pTextBlock->setFontSize(16);
	pTextBlock->setFontColor(Color(Colors::Red));
	//pTextBlock->disableEditing();
	//pTextBlock->setMargin(Margin(5,0,0,0));
  //labelBlock->setMargin(Margin(20));
	pTextBlock->setBorder(Border(1, Color(Colors::Grey))); // red border

	pBorderLayout->addBlock(pTextBlock, UI::Layout::BorderLayout::Position::Center);

/*-------------------------------------------------west block-----------------------------------------------------------------*/
	labelBlock = new UI::Controls::Label(wpCustomWindow, UIRect(150, 75), UIRect(50, 50), Spacing::HORIZONTAL);
	labelBlock->setBackgroundColor(Color(Colors::LightGreen));
	labelBlock->setText("Lorem Ipsum is simply dummy text of the printing and typesetting industry. ");
	labelBlock->setFont("vera");
	labelBlock->setFontSize(16);
	labelBlock->setFontColor(Color(Colors::Black));
	//labelBlock->setMargin(Margin(5,0,0,0));
  //labelBlock->setMargin(Margin(20));
	labelBlock->setBorder(Border(1, Color(Colors::Grey))); // red border

	pBorderLayout->addBlock(labelBlock, UI::Layout::BorderLayout::Position::West);

	UI::Block * pBlock;
	UI::Layout::BorderLayout * pCenterBorderLayout;
	pBlock = new UI::Block(wpCustomWindow, UIRect(130, 200), UIRect(50, 50), Spacing::VERTICAL);
	pBlock->getLayout()->setPositioning(Layout::BoxLayout::VERTICAL);
	//pCenterBorderLayout = new UI::Layout::BorderLayout(pBlock);
	//pBlock->setLayout(pCenterBorderLayout);
	pBlock->setBackgroundColor(Color(Colors::LightPurple)); // visible margin... TODO fix this bug
	
	pBorderLayout->addBlock(pBlock, UI::Layout::BorderLayout::Position::West);

	//setup label for center block
	pTextBlock = new UI::Controls::TextBlock(wpCustomWindow, UIRect(120, 150), UIRect(50, 50), Spacing::HORIZONTAL);
	pTextBlock->setBackgroundColor(Color(112, 255, 0));
	pTextBlock->setText("Same block as in center but with ability to edit text");
	pTextBlock->setFont("vera");
	pTextBlock->setFontSize(16);
	pTextBlock->setFontColor(Color(255, 255, 255));
	pTextBlock->enableEditing();
  //labelBlock->setMargin(Margin(20));
  pTextBlock->setMargin(Margin(5,0,0,0));
	pTextBlock->setBorder(Border(1, Color(Colors::LightGrey)));
	pTextBlock->setPadding(Padding(5,0));

	pBlock->addChild(pTextBlock);

	pTextBlock = new UI::Controls::TextBlock(wpCustomWindow, UIRect(100, 150), UIRect(50, 50), Spacing::HORIZONTAL);
	pTextBlock->setBackgroundColor(Color(112, 255, 0));
	pTextBlock->setText("Same block as above  but without ability to edit text but with different font");
	pTextBlock->setFont("obelixpro");
	pTextBlock->setFontSize(16);
	pTextBlock->setFontColor(Color(255, 255, 255));
	//pTextBlock->enableEditing();
  //labelBlock->setMargin(Margin(20));
  pTextBlock->setMargin(Margin(5,0,0,0));
	pTextBlock->setBorder(Border(1, Color(Colors::LightGrey)));
	pTextBlock->setPadding(Padding(5,0));

	pBlock->addChild(pTextBlock);
/*-------------------------------------------------east block-----------------------------------------------------------------*/

	labelBlock = new UI::Controls::Label(wpCustomWindow, UIRect(150, 75), UIRect(50, 50), Spacing::HORIZONTAL);
	labelBlock->setBackgroundColor(Color(Colors::LightGreen));
	labelBlock->setText("East block");
	labelBlock->setFont("vera");
	labelBlock->setFontSize(16);
	labelBlock->setFontColor(Color(Colors::DarkGrey));
	//labelBlock->setMargin(Margin(0,0,0,0));
	labelBlock->setBorder(Border(1, Color(Colors::Grey))); // red border
	pBorderLayout->addBlock(labelBlock, UI::Layout::BorderLayout::Position::East);

  //UI::Block * pBlock;
	//UI::Layout::BorderLayout * pCenterBorderLayout;
	pBlock = new UI::Block(wpCustomWindow, UIRect(130, 200), UIRect(50, 50), Spacing::VERTICAL);
	pBlock->getLayout()->setPositioning(Layout::BoxLayout::VERTICAL);
	//pCenterBorderLayout = new UI::Layout::BorderLayout(pBlock);
	//pBlock->setLayout(pCenterBorderLayout);
	pBlock->setBackgroundColor(Color(Colors::LightPurple)); // visible margin... TODO fix this bug
	
	pBorderLayout->addBlock(pBlock, UI::Layout::BorderLayout::Position::East);

	UI::Controls::ImageBlock * pImageBlock;
	//setup label for center block
	pImageBlock = new UI::Controls::ImageBlock(wpCustomWindow, UIRect(120, 150), UIRect(50, 50), Spacing::HORIZONTAL);
	pImageBlock->setText("East block with textured background and bold font");
	pImageBlock->setFont("veramobd");
	pImageBlock->setFontSize(14);
	pImageBlock->setFontColor(Color(Colors::Black));
	pImageBlock->setBackgroundImage("logo.bmp");
  //labelBlock->setMargin(Margin(20));
  pImageBlock->setMargin(Margin(2));
	pImageBlock->setBorder(Border(2, Color(Colors::LightGrey)));
	//pImageBlock->setPadding(Padding(5,0));

	pBlock->addChild(pImageBlock);


	UI::Controls::FPSLabel * fpsBlock;

	fpsBlock = new UI::Controls::FPSLabel(wpCustomWindow, UIRect(100, 50), UIRect(50, 50), Spacing::HORIZONTAL);
	fpsBlock->setMaximumSize(UIRect(100, 300));
	fpsBlock->setBackgroundColor(Color(112, 255, 0));
	//fpsBlock->setText("Testukas2");
	fpsBlock->setFont("vera");
	fpsBlock->setFontSize(32);
	fpsBlock->setFontColor(Color(255, 255, 0));

  fpsBlock->setBorder(Border(3, Color(0,0,0)));

	pBlock->addChild(fpsBlock);

/*-------------------------------------------------North block-----------------------------------------------------------------*/

	UI::Block * pNorthBlock;

	pNorthBlock = new UI::Block(wpCustomWindow, UIRect(150, 30), UIRect(0, 0), Spacing::HORIZONTAL);// TODO bug width preferred sizes
	pNorthBlock->setBackgroundColor(Color(204, 255, 204));
	//pNorthBlock->setText("North block");
	//pNorthBlock->setFont("vera");
	//pNorthBlock->setFontSize(16);
	//pNorthBlock->setFontColor(Color(255, 255, 255));
	labelBlock->setMargin(Margin(5,0,0,0));
	labelBlock->setBorder(Border(3, Color(Colors::Grey))); // red border

	pBorderLayout->addBlock(pNorthBlock, UI::Layout::BorderLayout::Position::North);

	// generating button test
	UI::Controls::ToggleButton* pButton;

	std::string buttonLabel = "Click me No.";
	for(int i = 0; i < 5; i++)
	{
		pButton = new UI::Controls::ToggleButton(wpCustomWindow, UIRect(100, 25), UIRect(0,0), Spacing::HORIZONTAL);
		pButton->setBackgroundColor(Color(Colors::LightYellow));
		pButton->setEnabledColor(Color(Colors::LightGreen));
		pButton->setDisabledColor(Color(Colors::LightYellow));
		pButton->setText("South block");
		pButton->setFont("vera");
		pButton->setFontSize(11);
		pButton->setBorder(Border(1, Color(Colors::Grey)));
		pButton->setText(buttonLabel);
		pButton->setText(pButton->getText().append(std::to_string(i)));
		pNorthBlock->getLayout()->addBlock(pButton);
	}

/*-------------------------------------------------south block-----------------------------------------------------------------*/
	UI::Block * pSouthBlock = new UI::Block(wpCustomWindow, UIRect(100, 30), UIRect(0,0), Spacing::HORIZONTAL); // TODO bug width preferred sizes
	
	pSouthBlock->setBackgroundColor(Color(Colors::LightGreen));
	//labelBlock->setMargin(Margin();
	pSouthBlock->setBorder(Border(1, Colors::Grey)); // red border

	pBorderLayout->addBlock(pSouthBlock, UI::Layout::BorderLayout::Position::South);

	UI::Controls::Label* pBottomLabel;

	std::string testLabel = "Label No.";
	for(int i = 0; i < 3; i++)
	{
		pBottomLabel = new UI::Controls::Label(wpCustomWindow, UIRect(90, 25), UIRect(0,0), Spacing::HORIZONTAL);
		pBottomLabel->setBackgroundColor(Color(Colors::LightYellow));
		pBottomLabel->setFont("vera");
		pBottomLabel->setFontSize(14);
		pBottomLabel->setBorder(Border(1, Color(Colors::Grey))); // red border
		pBottomLabel->setText(testLabel);
		pBottomLabel->setMargin(Margin(5));
		pBottomLabel->setText(pBottomLabel->getText().append(std::to_string(i)));

		pSouthBlock->getLayout()->addBlock(pBottomLabel);
	}

/*-------------------------------------------------END OF LAYOUT CREATION------------------------------------------------------------*/
	if (customWindow->getGWindow().get() == nullptr) {
		std::cout << "could not create main window\n";
		exit(-1);
	}
    
  mainBlock->invalidate(); // update window

	//TODO split to below to different method
	m_timer = m_system->installTimer(0 /*delay*/, 20 /*interval*/, timer, customWindow->getGWindow().get());
}

bool Application::processEvent(GHOST_IEvent *event)
{
	GHOST_IWindow *window = event->getWindow();
	bool handled = true;

	switch (event->getType()) {
 
 		/*
 			GHOST_kEventCursorMove,     /// Mouse move event
			GHOST_kEventButtonDown,     /// Mouse button event
			GHOST_kEventButtonUp,       /// Mouse button event
			GHOST_kEventWheel,          /// Mouse wheel event
 		*/

		/*
		typedef struct {
			GHOST_TButtonMask button;
		} GHOST_TEventButtonData;
		*/
		case GHOST_kEventWheel:
		{
			//mouseDriver->handleWheel((GHOST_TEventWheelData *) event->getData());
		}
		break;
		
		case GHOST_kEventButtonDown:
		{
		GHOST_IWindow *window2 = event->getWindow();
			if (!m_system->validWindow(window2))
				break;

			int cx, cy; // cursor x,y positions
			m_system->getCursorPosition(cx, cy);

			GHOST_TEventButtonData * eventData = (GHOST_TEventButtonData *) event->getData();
			
			Events::MouseEventDataPtr pMouseEventData(new Events::MouseEventData(cx, cy, eventData->button, 0));

			WindowPtr pGWindow(pWindowManager->getWindow(window2));
			if(pGWindow)
			{
				printf("Triggering mouse event from ghost \n");
				pGWindow->getMouseDriver()->fireEvent(pMouseEventData);
			} else {
				printf("WindowManager::getGWindow() could not find ghost window \n");		
			}
		}
		break;
		
		case GHOST_kEventButtonUp:
		{
			//mouseDriver->handleButtonUp((GHOST_TEventButtonData *) event->getData());
		}
		break;
		
		case GHOST_kEventCursorMove:
		{
			//mouseDriver->handleCursorMove((GHOST_TEventCursorData *) event->getData());
		} 
		break;
		
		case GHOST_kEventKeyUp:
		{
			GHOST_IWindow *window2 = event->getWindow();
			if (!m_system->validWindow(window2))
				break;

			GHOST_TEventKeyData * eventData = (GHOST_TEventKeyData *) event->getData();

			Events::KeyEventDataPtr pKeyEventData(new Events::KeyEventData(new GHOST_TEventKeyData(*eventData), false/*up*/)); // making new copy
			WindowPtr pGWindow(pWindowManager->getWindow(window2));
			if(pGWindow)
			{
				printf("Triggering key up event from ghost \n");
				pGWindow->getKeyDriver()->fireEvent(pKeyEventData);
			} else {
				printf("WindowManager::getGWindow() could not find ghost window \n");		
			}
		}
		break;
		case GHOST_kEventKeyDown:
		{
			GHOST_IWindow *window2 = event->getWindow();
			if (!m_system->validWindow(window2))
				break;

			GHOST_TEventKeyData * eventData = (GHOST_TEventKeyData *) event->getData();

			Events::KeyEventDataPtr pKeyEventData(new Events::KeyEventData(new GHOST_TEventKeyData(*eventData), true/*down*/)); // making new copy
			WindowPtr pGWindow(pWindowManager->getWindow(window2));
			if(pGWindow)
			{
				printf("Triggering key down event from ghost \n");
				pGWindow->getKeyDriver()->fireEvent(pKeyEventData);
			} else {
				printf("WindowManager::getGWindow() could not find ghost window \n");		
			}

			break;
		}
		break;

		case GHOST_kEventWindowSize:
		{
			printf("Resolution changed \n"); // not nessesary
      GHOST_IWindow *window2 = event->getWindow();
      GHOST_Rect bnds;
			window->getClientBounds(bnds);
      glViewport(0, 0, bnds.getWidth(), bnds.getHeight());
      
		} break;

		case GHOST_kEventWindowClose:
		{
			GHOST_IWindow *window2 = event->getWindow();
			if (window2 == customWindow->getGWindow().get()) {
				m_exitRequested = true;
			}
			else {
				m_system->disposeWindow(window2);
			}
		}
		break;

		case GHOST_kEventWindowActivate:
			handled = false;
			break;

		case GHOST_kEventWindowDeactivate:
			handled = false;
			break;

		case GHOST_kEventWindowUpdate: // most likely resoze
		{
			GHOST_IWindow *window2 = event->getWindow();
			if (!m_system->validWindow(window2))
				break;


			handled = pWindowManager->updateWindow(window2);
			//drawBitmap(); // must be inside update window because it swaps buffers and we cannot paint anymore
			break;
		}
		break;
		
		default:
			handled = false;
			break;
	}
	return handled;
}

int main(int /*argc*/, char ** /*argv*/)
{
	// Create the system
	GHOST_ISystem::createSystem();
	fSystem = GHOST_ISystem::getSystem();
	GLenum glew_status;


	if (fSystem) {
		// Create an application object
		Application app(fSystem);
		//WARNING before initializing glew window must be created else we get missing gl error

		if(!fSystem)
		{
			printf("Something happened width Ghost system handle \n");		
		}
		// after OpenGL was initialized we can init glew and start font manager
		printf("Initializing glew \n");

		glew_status = glewInit();

		if (GLEW_OK != glew_status) {
			fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
			return 1;
		}

		if (!GLEW_VERSION_2_0) {
			fprintf(stderr, "No support for OpenGL 2.0 found\n");
			return 1;
		}
		printf("Initializing application resources \n");
		app.initResources(); // initializing resources that need glew
		fm = app.pWindowManager->getFontManager().get();
		printf("Creating layout \n");	
		app.createLayout();
		printf("Layout created\n");

		// Add the application as event consumer
		fSystem->addEventConsumer(&app);
		// Enter main loop
		while (!app.m_exitRequested) {
			fSystem->processEvents(true);
			// event manager processes Ghost system events
			fSystem->dispatchEvents();
			//triggering uighost events
			app.pWindowManager->triggerQuenedEvents();
		}
	}

	

	// Dispose the system
	GHOST_ISystem::disposeSystem();

	return 0;
}

static void UI::timer(GHOST_ITimerTask *task, GHOST_TUns64 /*time*/)
{
	GHOST_IWindow *window = (GHOST_IWindow *)task->getUserData();
	if (fSystem->validWindow(window)) {
		window->invalidate();
	}
}



/*
static std::vector<std::string>
UI::getFontList()
{
	namespace fs = boost::filesystem;
	fs::path someDir("./fonts");
	fs::directory_iterator end_iter;

	typedef std::vector<fs::path> result_set_t;
	result_set_t result_set;

	if ( fs::exists(someDir) && fs::is_directory(someDir))
	{
		for( fs::directory_iterator dir_iter(someDir) ; dir_iter != end_iter ; ++dir_iter)
		{
		  if (fs::is_regular_file(dir_iter->status()) )
		  {
		    result_set.push_back(*dir_iter);
			}
		}
	}

	std::vector<std::string> files;
	for(result_set_t::iterator it = result_set.begin(); it != result_set.end(); it++)
	{
		files.push_back(it->string());
	}	

	return files;
}*/
