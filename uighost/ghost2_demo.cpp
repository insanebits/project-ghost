#include "ui_api.h"

using namespace UI;

Application::Application(GHOST_ISystem *system)
	: m_system(system), m_timer(0), m_cursor(GHOST_kStandardCursorFirstCursor),
	m_exitRequested(false), stereo(false)
{
	using namespace Events;

	fApp = this;
	IEventProducerPtr pEventProducer;
	IEventListenerPtr pEventListener;
	
	//Event manager initialization
	wpEventManager = pEventManager; // weak pointer can be assigned
	// key driver initialization and it's event type registration
	//keyDriver.reset(new Events::KeyDriver(wpEventManager));

	pWindowManager = new UI::WindowManager(m_system);
	// needs to be created before GLEW is initialized
	customWindow = pWindowManager->createWindow("Custom window", 0, 0, 640, 480).get();
}


Application::~Application(void)
{
	// Dispose windows
//if (m_system->validWindow(customWindow->getGWindow().get())) {
//		m_system->disposeWindow(customWindow->getGWindow().get());
		//delete customWindow; 
//	}
}

void 
Application::initResources()
{
	if(!m_system)
	{
		printf("GHOST system does not exit \n");		
	}
	std::vector<std::string> files;
	files.push_back("/home/insane/Desktop/opengl/ghost_separated/build/bin/fonts/Vera.ttf");

	pFontManager.reset(new FontManager(files));
	pWindowManager->setFontManager(pFontManager);
}

void
Application::createLayout()
{

	WindowPtr pCustomWindow = pWindowManager->getWindow(customWindow->getGWindow().get()); // hacky!!
	WindowWPtr wpCustomWindow = pCustomWindow;

	UI::Layout::ILayout * pLayout;
	UI::Layout::BorderLayout * pBorderLayout;


	IBlockPtr mainBlock = customWindow->getBlock();
	pLayout = new UI::Layout::BorderLayout(mainBlock.get());
	pBorderLayout = (UI::Layout::BorderLayout *) pLayout; // not sure if works;
	//mainBlock->getLayout()->setPositioning(Spacing::VERTICAL);
	mainBlock->setLayout(pLayout);
	mainBlock->setBackgroundColor(Color(200, 255, 255));
	mainBlock->setPadding(Padding(10));

	
	UI::Controls::Label * labelBlock;

/*-------------------------------------------------center block-----------------------------------------------------------------*/
	
	UI::Block * pBlock;
	UI::Layout::BorderLayout * pCenterBorderLayout;
	pBlock = new UI::Block(wpCustomWindow, UIRect(600, 100), UIRect(50, 50), Spacing::HORIZONTAL);
	pCenterBorderLayout = new UI::Layout::BorderLayout(pBlock);
	pBlock->setLayout(pCenterBorderLayout);
	//pBlock->setBackgroundColor(Color(80, 100, 200)); // visible margin... TODO fix this bug
	
	pBorderLayout->addBlock(pBlock, UI::Layout::BorderLayout::Position::Center);

	UI::Controls::TextBlock * pTextBlock;
	//setup label for center block
	pTextBlock = new UI::Controls::TextBlock(wpCustomWindow, UIRect(300, 100), UIRect(50, 50), Spacing::HORIZONTAL);
	pTextBlock->setBackgroundColor(Color(112, 255, 0));
	pTextBlock->setText("Center block with lots of text");
	pTextBlock->setFont("vera");
	pTextBlock->setFontSize(16);
	pTextBlock->setFontColor(Color(255, 255, 255));
  //labelBlock->setMargin(Margin(20));
  pTextBlock->setMargin(Margin(5,0,0,0));
	pTextBlock->setBorder(Border(3, Color(0,0,0)));
	pTextBlock->setPadding(Padding(5,0));

	pCenterBorderLayout->addBlock(pTextBlock, UI::Layout::BorderLayout::Position::Center);

	labelBlock = new UI::Controls::Label(wpCustomWindow, UIRect(100, 100), UIRect(50, 50), Spacing::HORIZONTAL);
	labelBlock->setBackgroundColor(Color(112, 255, 0));
	labelBlock->setText("Inner west block");
	labelBlock->setFont("vera");
	labelBlock->setFontSize(12);
	labelBlock->setFontColor(Color(255, 255, 255));
  //labelBlock->setMargin(Margin(20));
  labelBlock->setMargin(Margin(5,0,0,0));
	labelBlock->setBorder(Border(3, Color(0,0,0)));
	labelBlock->setPadding(Padding(5,0));

	pCenterBorderLayout->addBlock(labelBlock, UI::Layout::BorderLayout::Position::West);

	labelBlock = new UI::Controls::Label(wpCustomWindow, UIRect(100, 100), UIRect(50, 50), Spacing::HORIZONTAL);
	labelBlock->setBackgroundColor(Color(112, 255, 0));
	labelBlock->setText("Inner east block");
	labelBlock->setFont("vera");
	labelBlock->setFontSize(14);
	labelBlock->setFontColor(Color(255, 255, 255));
  labelBlock->setMargin(Margin(20));
  labelBlock->setMargin(Margin(5,0,0,0));
	labelBlock->setBorder(Border(3, Color(0,0,0)));
	labelBlock->setPadding(Padding(5,0));

	pCenterBorderLayout->addBlock(labelBlock, UI::Layout::BorderLayout::Position::East);

/*-------------------------------------------------corners-----------------------------------------------------------------*/
	labelBlock = new UI::Controls::Label(wpCustomWindow, UIRect(150, 75), UIRect(50, 50), Spacing::HORIZONTAL);
	labelBlock->setBackgroundColor(Color(75, 48, 16));
	labelBlock->setText("West block");
	labelBlock->setFont("vera");
	labelBlock->setFontSize(16);
	labelBlock->setFontColor(Color(255, 255, 255));
	labelBlock->setMargin(Margin(5,0,0,0));
  //labelBlock->setMargin(Margin(20));
	labelBlock->setBorder(Border(3, Color(255, 0, 0))); // red border

	pBorderLayout->addBlock(labelBlock, UI::Layout::BorderLayout::Position::West);
	
	labelBlock = new UI::Controls::Label(wpCustomWindow, UIRect(150, 75), UIRect(50, 50), Spacing::HORIZONTAL);
	labelBlock->setBackgroundColor(Color(75, 48, 16));
	labelBlock->setText("East block");
	labelBlock->setFont("vera");
	labelBlock->setFontSize(16);
	labelBlock->setFontColor(Color(255, 255, 255));
	labelBlock->setMargin(Margin(5,0,0,0));
	labelBlock->setBorder(Border(3, Color(255, 0, 0))); // red border

	pBorderLayout->addBlock(labelBlock, UI::Layout::BorderLayout::Position::East);

	labelBlock = new UI::Controls::Label(wpCustomWindow, UIRect(150, 75), UIRect(50, 50), Spacing::HORIZONTAL);
	labelBlock->setBackgroundColor(Color(204, 255, 204));
	labelBlock->setText("North block");
	labelBlock->setFont("vera");
	labelBlock->setFontSize(16);
	labelBlock->setFontColor(Color(255, 255, 255));
	labelBlock->setMargin(Margin(5,0,0,0));
	labelBlock->setBorder(Border(3, Color(255, 0, 0))); // red border

	pBorderLayout->addBlock(labelBlock, UI::Layout::BorderLayout::Position::North);

	labelBlock = new UI::Controls::Label(wpCustomWindow, UIRect(150, 75), UIRect(50, 50), Spacing::VERTICAL);
	labelBlock->setBackgroundColor(Color(204, 255, 204));
	labelBlock->setText("South block");
	labelBlock->setFont("vera");
	labelBlock->setFontSize(16);
	labelBlock->setFontColor(Color(255, 255, 255));
	labelBlock->setMargin(Margin(5,0,0,0));
	labelBlock->setBorder(Border(3, Color(255, 0, 0))); // red border

	pBorderLayout->addBlock(labelBlock, UI::Layout::BorderLayout::Position::South);


/*-------------------------------------------------fps label test---------------------------------------------------------------*/
	/*UI::Controls::FPSLabel * fpsBlock;

	fpsBlock = new UI::Controls::FPSLabel(wpCustomWindow, UIRect(100, 50), UIRect(50, 50), Spacing::HORIZONTAL);
	fpsBlock->setMaximumSize(UIRect(100, 300));
	fpsBlock->setBackgroundColor(Color(112, 255, 0));
	//fpsBlock->setText("Testukas2");
	fpsBlock->setFont("vera");
	fpsBlock->setFontSize(32);
	fpsBlock->setFontColor(Color(255, 255, 0));

  fpsBlock->setBorder(Border(3, Color(0,0,0)));

	mainBlock->addChild(fpsBlock);*/
/*-------------------------------------------------toggle button test------------------------------------------------------------*/
	/*UI::Controls::ToggleButton * toggleButton;

	toggleButton = new UI::Controls::ToggleButton(wpCustomWindow, UIRect(100, 50), UIRect(50, 50), Spacing::HORIZONTAL);
	toggleButton->setMaximumSize(UIRect(100, 200));
	toggleButton->setEnabledColor(Color(255,0,0)); // red
	toggleButton->setDisabledColor(Color(0,0,255)); // blue
	toggleButton->setFont("vera");
	toggleButton->setFontSize(32);
	toggleButton->setFontColor(Color(255, 255, 0));
  toggleButton->setMargin(Margin(10));
	toggleButton->setPadding(Padding(5,0,0,5));
	toggleButton->setBorder(Border(2, Color(55,0,0)));

	mainBlock->addChild(toggleButton);*/

/*-------------------------------------------------textfield test-----------------------------------------------------------------*/
/*	UI::Controls::TextField * pTextField;

	pTextField = new UI::Controls::TextField(wpCustomWindow, UIRect(100, 50), UIRect(50, 50), Spacing::HORIZONTAL);
	pTextField->setFont("vera");
	pTextField->setFontSize(32);
	pTextField->setFontColor(Color(255, 150, 0));
	pTextField->setBackgroundColor(Color(255, 255, 255));
	pTextField->setPadding(Padding(5,0,0,5));
	pTextField->setBorder(Border(2, Color(55,0,0)));
	pTextField->setText("Try editing this");

	mainBlock->addChild(pTextField);*/

/*-------------------------------------------------END OF LAYOUT CREATION------------------------------------------------------------*/
	if (customWindow->getGWindow().get() == nullptr) {
		std::cout << "could not create main window\n";
		exit(-1);
	}

	//TODO split to below to different method
	m_timer = m_system->installTimer(0 /*delay*/, 20 /*interval*/, timer, customWindow->getGWindow().get());
}

bool Application::processEvent(GHOST_IEvent *event)
{
	GHOST_IWindow *window = event->getWindow();
	bool handled = true;

	switch (event->getType()) {
 
 		/*
 			GHOST_kEventCursorMove,     /// Mouse move event
			GHOST_kEventButtonDown,     /// Mouse button event
			GHOST_kEventButtonUp,       /// Mouse button event
			GHOST_kEventWheel,          /// Mouse wheel event
 		*/

		/*
		typedef struct {
			GHOST_TButtonMask button;
		} GHOST_TEventButtonData;
		*/
		case GHOST_kEventWheel:
		{
			//mouseDriver->handleWheel((GHOST_TEventWheelData *) event->getData());
		}
		break;
		
		case GHOST_kEventButtonDown:
		{
		GHOST_IWindow *window2 = event->getWindow();
			if (!m_system->validWindow(window2))
				break;

			int cx, cy; // cursor x,y positions
			m_system->getCursorPosition(cx, cy);

			GHOST_TEventButtonData * eventData = (GHOST_TEventButtonData *) event->getData();
			
			Events::MouseEventDataPtr pMouseEventData(new Events::MouseEventData(cx, cy, eventData->button, 0));

			WindowPtr pGWindow(pWindowManager->getWindow(window2));
			if(pGWindow)
			{
				printf("Triggering mouse event from ghost \n");
				pGWindow->getMouseDriver()->fireEvent(pMouseEventData);
			} else {
				printf("WindowManager::getGWindow() could not find ghost window \n");		
			}
		}
		break;
		
		case GHOST_kEventButtonUp:
		{
			//mouseDriver->handleButtonUp((GHOST_TEventButtonData *) event->getData());
		}
		break;
		
		case GHOST_kEventCursorMove:
		{
			//mouseDriver->handleCursorMove((GHOST_TEventCursorData *) event->getData());
		} 
		break;
		
		case GHOST_kEventKeyUp:
		{
			GHOST_IWindow *window2 = event->getWindow();
			if (!m_system->validWindow(window2))
				break;

			GHOST_TEventKeyData * eventData = (GHOST_TEventKeyData *) event->getData();

			Events::KeyEventDataPtr pKeyEventData(new Events::KeyEventData(new GHOST_TEventKeyData(*eventData), false/*up*/)); // making new copy
			WindowPtr pGWindow(pWindowManager->getWindow(window2));
			if(pGWindow)
			{
				printf("Triggering event from ghost \n");
				pGWindow->getKeyDriver()->fireEvent(pKeyEventData);
			} else {
				printf("WindowManager::getGWindow() could not find ghost window \n");		
			}
		}
		break;
		case GHOST_kEventKeyDown:
		{
			GHOST_IWindow *window2 = event->getWindow();
			if (!m_system->validWindow(window2))
				break;

			GHOST_TEventKeyData * eventData = (GHOST_TEventKeyData *) event->getData();

			Events::KeyEventDataPtr pKeyEventData(new Events::KeyEventData(new GHOST_TEventKeyData(*eventData), true/*down*/)); // making new copy
			WindowPtr pGWindow(pWindowManager->getWindow(window2));
			if(pGWindow)
			{
				printf("Triggering event from ghost \n");
				pGWindow->getKeyDriver()->fireEvent(pKeyEventData);
			} else {
				printf("WindowManager::getGWindow() could not find ghost window \n");		
			}

			break;
		}
		break;

		case GHOST_kEventWindowSize:
		{
			printf("Resolution changed \n");
      GHOST_IWindow *window2 = event->getWindow();
      GHOST_Rect bnds;
			window->getClientBounds(bnds);
      glViewport(0, 0, bnds.getWidth(), bnds.getHeight());
      
		} break;

		case GHOST_kEventWindowClose:
		{
			GHOST_IWindow *window2 = event->getWindow();
			if (window2 == customWindow->getGWindow().get()) {
				m_exitRequested = true;
			}
			else {
				m_system->disposeWindow(window2);
			}
		}
		break;

		case GHOST_kEventWindowActivate:
			handled = false;
			break;

		case GHOST_kEventWindowDeactivate:
			handled = false;
			break;

		case GHOST_kEventWindowUpdate: // most likely resoze
		{
			GHOST_IWindow *window2 = event->getWindow();
			if (!m_system->validWindow(window2))
				break;


			handled = pWindowManager->updateWindow(window2);
			break;

			WindowPtr spWindow = pWindowManager->getWindow(window2);
			if(!spWindow)
			{
				break;			
			}


			if(spWindow.get() == customWindow2)
			{
				spWindow->getGWindow()->activateDrawingContext();
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				/* White background */
				glClearColor(1, 1, 1, 1); // background white
				// update window
				spWindow->getGWindow()->swapBuffers();
				break;
			}
		}
		break;
		
		default:
			handled = false;
			break;
	}
	return handled;
}

int main(int /*argc*/, char ** /*argv*/)
{
	// Create the system
	GHOST_ISystem::createSystem();
	fSystem = GHOST_ISystem::getSystem();
	GLenum glew_status;


	if (fSystem) {
		// Create an application object
		Application app(fSystem);
		//WARNING before initializing glew window must be created else we get missing gl error

		if(!fSystem)
		{
			printf("Something happened width Ghost system handle \n");		
		}
		// after OpenGL was initialized we can init glew and start font manager
		printf("Initializing glew \n");

		glew_status = glewInit();

		if (GLEW_OK != glew_status) {
			fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
			return 1;
		}

		if (!GLEW_VERSION_2_0) {
			fprintf(stderr, "No support for OpenGL 2.0 found\n");
			return 1;
		}
		printf("Initializing application resources \n");
		app.initResources(); // initializing resources that need glew
		fm = app.pWindowManager->getFontManager().get();
		printf("Creating layout \n");	
		app.createLayout();
		printf("Layout created\n");

		// Add the application as event consumer
		fSystem->addEventConsumer(&app);
		// Enter main loop
		while (!app.m_exitRequested) {
			fSystem->processEvents(true);
			// event manager processes Ghost system events
			fSystem->dispatchEvents();
			//triggering uighost events
			app.pWindowManager->triggerQuenedEvents();
		}
	}

	// Dispose the system
	GHOST_ISystem::disposeSystem();

	return 0;
}

static void UI::timer(GHOST_ITimerTask *task, GHOST_TUns64 /*time*/)
{
	GHOST_IWindow *window = (GHOST_IWindow *)task->getUserData();
	if (fSystem->validWindow(window)) {
		window->invalidate();
	}
}



/*
static std::vector<std::string>
UI::getFontList()
{
	namespace fs = boost::filesystem;
	fs::path someDir("./fonts");
	fs::directory_iterator end_iter;

	typedef std::vector<fs::path> result_set_t;
	result_set_t result_set;

	if ( fs::exists(someDir) && fs::is_directory(someDir))
	{
		for( fs::directory_iterator dir_iter(someDir) ; dir_iter != end_iter ; ++dir_iter)
		{
		  if (fs::is_regular_file(dir_iter->status()) )
		  {
		    result_set.push_back(*dir_iter);
			}
		}
	}

	std::vector<std::string> files;
	for(result_set_t::iterator it = result_set.begin(); it != result_set.end(); it++)
	{
		files.push_back(it->string());
	}	

	return files;
}*/
