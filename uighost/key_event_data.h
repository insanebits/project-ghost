#ifndef KEY_EVENT_DATA_H
#define KEY_EVENT_DATA_H
#include "ui_ghost_macros.h"

namespace Events{
	class KeyEventData:public IEventArgs{
	public:
		KeyEventData(){}
		KeyEventData(GHOST_TEventKeyData * data, bool down):m_pGhostKeyData(data), m_Down(down)
		{}
		//TODO handle event data safe
		//TODO remove this method use 

		DEPRECATED(GHOST_TEventKeyData * getData());
		GHOST_TEventKeyData * getGData()
		{
			return getData();
		}
		bool isDown(){return m_Down;}
	private: 
		GHOST_TEventKeyData * m_pGhostKeyData;//gives data which key triggered event
		bool m_Down; // to know button up or down event
	};

		GHOST_TEventKeyData * 
		KeyEventData::getData()
		{
			return m_pGhostKeyData;
		}
	
}
#endif
