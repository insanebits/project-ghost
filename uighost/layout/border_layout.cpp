#include "border_layout.h"
#include "ui_api.h" 
#include <stdio.h>
#include "ui_rect.h"

using namespace UI::Layout;

bool 
BorderLayout::invalidate()
{
	//printf("BorderLayout::%s() started invalidation\n", __func__);
	int widthUsed = 0;// sizes used by corner blocks
	int heightUsed = 0;
	WindowPtr pWindow;
	UIRect centerBlockPosition;
	IBlock * pBlock = this->block;	
	UIRect childPosition;

	// checking prerequisities
	if(!pBlock)
	{
		printf("BorderLayout::%s() invalid main block\n", __func__);
		return false;	
	}

	if(!(pWindow = pBlock->getWindow().lock()))
	{
		printf("BorderLayout::%s() invalid block window\n", __func__);
		return false;	
	}

	centerBlockPosition = pBlock->getPosition(); // max it can be is main block size

	//first let's invalidate corner blocks to know how much space
	// north and south position should be calculated first because they're needed by west and east

	if(m_pNorthBlock)
	{
		childPosition = m_pNorthBlock->getPosition();
		childPosition.width =  pBlock->getPosition().width; // width to prefered size
		childPosition.height = m_pNorthBlock->getPreferredSize().height;
		m_pNorthBlock->setPosition(childPosition); // setting correct width and height 
		
		// now we can invalidate
		m_pNorthBlock->invalidate();		
    UIRect pos = m_pNorthBlock->getPosition();	// for gdb..
		heightUsed += m_pNorthBlock->getPosition().height;	
    //printf("BorderLayout::%s() north height %d\n", __func__, m_pNorthBlock->getPosition().height);
		centerBlockPosition.coordY += m_pNorthBlock->getPosition().height; // center block starts after north vertical
	}

	if(m_pSouthBlock)
	{
		childPosition = m_pSouthBlock->getPosition();
		childPosition.width =  pBlock->getPosition().width; // width to prefered size
		childPosition.height = m_pSouthBlock->getPreferredSize().height;

		m_pSouthBlock->setPosition(childPosition); // setting correct width and height 
    m_pSouthBlock->invalidate();	
    UIRect pos2 = m_pSouthBlock->getPosition();	// for gdb..
    //printf("BorderLayout::%s() north height %d\n", __func__, m_pSouthBlock->getPosition().height);
		heightUsed += m_pSouthBlock->getPosition().height;	
		m_pSouthBlock->invalidate();	
	}

	if(m_pWestBlock)
	{
		childPosition = m_pWestBlock->getPosition();
		childPosition.width =  m_pWestBlock->getPreferredSize().width; // width to prefered size
		childPosition.height = pBlock->getPosition().height;
		// if set North block we need subtract it's height
		if(m_pNorthBlock)
		{
			childPosition.height -= m_pNorthBlock->getPosition().height; 
		}
		// if set South block we need subtract it's height
		if(m_pSouthBlock)
		{
			childPosition.height -= m_pSouthBlock->getPosition().height; 
		}
		m_pWestBlock->setPosition(childPosition); // setting correct width and height 
		// now we can invalidate
		m_pWestBlock->invalidate();		
		widthUsed += m_pWestBlock->getPosition().width;	
		centerBlockPosition.coordX += m_pWestBlock->getPosition().width; // center block starts after west horizontal
	}

	if(m_pEastBlock)
	{
		childPosition = m_pEastBlock->getPosition();
		childPosition.width =  m_pEastBlock->getPreferredSize().width; // width to prefered size
		childPosition.height = pBlock->getPosition().height;
		// if set North block we need subtract it's height
		if(m_pNorthBlock)
		{
			childPosition.height -= m_pNorthBlock->getPosition().height; 
		}
		// if set South block we need subtract it's height
		if(m_pSouthBlock)
		{
			childPosition.height -= m_pSouthBlock->getPosition().height; 
		}
		m_pEastBlock->setPosition(childPosition); // setting correct width and height 
		// now we can invalidate
		m_pEastBlock->invalidate();		
		widthUsed += m_pEastBlock->getPosition().width;	
	}

	// now we must calculate how much space left for out center block

  //printf("BorderLayout::%s() space used by corner blocks %d %d\n", __func__, widthUsed, heightUsed);
	centerBlockPosition.width = pBlock->getPosition().width - widthUsed;
	centerBlockPosition.height = pBlock->getPosition().height - heightUsed;
	
	if(m_pCenterBlock)
	{
		m_pCenterBlock->setPosition(centerBlockPosition); // setting where center block will be
		//pBlock->invalidate(); // recalculate by current position it's children
		m_pCenterBlock->invalidate();
	} else {
		//printf("BorderLayout::%s() center block does not exist\n", __func__);
	}

	// now we can set correct positions for children

	if(m_pNorthBlock) // starts from  parent x,y coordinates
	{
		UI::Insets childInsets = m_pNorthBlock->getInsets();

		childPosition = m_pNorthBlock->getPosition();
		childPosition.coordX = pBlock->getPosition().coordX + childInsets.getLeftInset();
		childPosition.coordY = pBlock->getPosition().coordY + childInsets.getTopInset();

		m_pNorthBlock->setPosition(childPosition);
		m_pNorthBlock->invalidate(); // recalculate children
	}

	if(m_pWestBlock)
	{
		UI::Insets childInsets = m_pWestBlock->getInsets();

		childPosition = m_pWestBlock->getPosition();

		childPosition.coordX = pBlock->getPosition().coordX + childInsets.getLeftInset();
		childPosition.coordY = pBlock->getPosition().coordY + childInsets.getTopInset();
		if(m_pNorthBlock)
		{
			childPosition.coordY += m_pNorthBlock->getPosition().height;
		}

		m_pWestBlock->setPosition(childPosition);
		m_pWestBlock->invalidate(); // recalculate children
	}

	if(m_pEastBlock)
	{
		UI::Insets childInsets = m_pEastBlock->getInsets();

		childPosition = m_pEastBlock->getPosition();

		childPosition.coordX = pBlock->getPosition().coordX + pBlock->getPosition().width;//setting to parent block end then we need to subtract east block width to know position
		childPosition.coordX -= childInsets.getRightInset();
		childPosition.coordX -= childPosition.width;

		childPosition.coordY = pBlock->getPosition().coordY + childInsets.getTopInset();
		if(m_pNorthBlock)
		{
			childPosition.coordY += m_pNorthBlock->getPosition().height;
		}

		m_pEastBlock->setPosition(childPosition);
		m_pEastBlock->invalidate(); // recalculate children
	}

	if(m_pSouthBlock)
	{
		UI::Insets childInsets = m_pSouthBlock->getInsets();

		childPosition = m_pSouthBlock->getPosition();
		childPosition.coordX = pBlock->getPosition().coordX + childInsets.getLeftInset();
		childPosition.coordY = pBlock->getPosition().coordY + pBlock->getPosition().height; // width and height should be correct from invalidate first
    childPosition.coordY -= childPosition.height + childInsets.getBottomInset();
		m_pSouthBlock->setPosition(childPosition);		

		m_pSouthBlock->invalidate(); // recalculate children
		printf("BorderLayout::%s() South block position %s \n", __func__, childPosition.toString().c_str());
	}

	return true;
}

/**
 * Method draws from starting coordinates
 */
void 
BorderLayout::drawLayout()
{
	std::list<IBlock *> * children;

	// first draw root block
	this->block->draw();

	// drawing children
	if(m_pWestBlock)
	{
		m_pWestBlock->getLayout()->drawLayout();
	}

	if(m_pNorthBlock)
	{
		m_pNorthBlock->getLayout()->drawLayout();
	}

	if(m_pEastBlock)
	{
		m_pEastBlock->getLayout()->drawLayout();
	}

	if(m_pSouthBlock)
	{
		m_pSouthBlock->getLayout()->drawLayout();
	}
	
	if(m_pCenterBlock)
	{
		m_pCenterBlock->getLayout()->drawLayout();
	} 
}

void 
BorderLayout::setPositioning(int p)
{
	// does nothing since we only have 5 positions and already know how to place them
}

/*
 * Method to add layout to block
 */
void
BorderLayout::addBlock(IBlock * block)
{
	addBlock(block, BorderLayout::Position::Center); // by default place in center
}

/*
 * Method to add layout to block
 */
void
BorderLayout::addBlock(IBlock * block, BorderLayout::Position position)
{
	std::list<IBlock *>* childList = this->block->getChildren();
	std::list<IBlock *>::iterator child; // used to store child position

	switch(position)
	{
		case Position::Center :
		{
			if(m_pCenterBlock)
			{
				child = std::find(childList->begin(), childList->end(), m_pCenterBlock);
				// find center block in block children list and remove
				if(child != childList->end())
				{
					childList->erase(child);
				}
				// now we can delete our child
				delete m_pCenterBlock;
			}

			m_pCenterBlock = block;
			childList->push_back(block); // adding new block in case someone will change layout
		} break;

		case Position::South :
		{
			if(m_pSouthBlock)
			{
				child = std::find(childList->begin(), childList->end(), m_pSouthBlock);
				// find center block in block children list and remove
				if(child != childList->end())
				{
					childList->erase(child);
				}
				// now we can delete our child
				delete m_pSouthBlock;
				childList->push_back(block); // adding new block in case someone will change layout
			}

			m_pSouthBlock = block;
		} break;

		case Position::North :
		{

			if(m_pNorthBlock)
			{
				child = std::find(childList->begin(), childList->end(), m_pNorthBlock);
				// find center block in block children list and remove
				if(child != childList->end())
				{
					childList->erase(child);
				}
				// now we can delete our child
				delete m_pNorthBlock;
				childList->push_back(block); // adding new block in case someone will change layout
			}

			m_pNorthBlock = block;
		} break;

		case Position::West :
		{			
			if(m_pWestBlock)
			{
				child = std::find(childList->begin(), childList->end(), m_pWestBlock);
				// find center block in block children list and remove
				if(child != childList->end())
				{
					childList->erase(child);
				}
				// now we can delete our child
				delete m_pWestBlock;
			}

			m_pWestBlock = block;
			childList->push_back(block); // adding new block in case someone will change layout
		} break;

		case Position::East :
		{
			if(m_pEastBlock)
			{
				child = std::find(childList->begin(), childList->end(), m_pEastBlock);
				// find center block in block children list and remove
				if(child != childList->end())
				{
					childList->erase(child);
				}
				// now we can delete our child
				delete m_pEastBlock;
			}

			m_pEastBlock = block;
			childList->push_back(block); // adding new block in case someone will change layout
		} break;
	
		default :
		{
			if(m_pCenterBlock)
			{
				child = std::find(childList->begin(), childList->end(), m_pCenterBlock);
				// find center block in block children list and remove
				if(child != childList->end())
				{
					childList->erase(child);
				}
				// now we can delete our child
				delete m_pCenterBlock;
			}

			m_pCenterBlock = block;
			childList->push_back(block); // adding new block in case someone will change layout
		} break;
	}
}

BorderLayout::BorderLayout(IBlock * b) : ILayout(b)
{
	m_pNorthBlock = nullptr;
	m_pSouthBlock = nullptr;
	m_pWestBlock = nullptr;
	m_pEastBlock = nullptr;
	m_pCenterBlock = nullptr;
}

void BorderLayout::reset()
{
	if(!this->block)
	{
		
		return;
	}
	
	std::list<UI::IBlock *>* childList = this->block->getChildren();
	if(childList->size() > 0)
	{
		addBlock(*(--childList->end())); // puting last element into center 
	}
}
