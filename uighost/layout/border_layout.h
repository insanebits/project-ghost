#ifndef UI_LAYOUT_BORDER_LAYOUT_H
#define UI_LAYOUT_BORDER_LAYOUT_H
#include <list> // maybe not needed
#include <math.h>
#include <iostream>
#include "ui_rect.h"
#include "ILayout.h"

namespace UI{
	namespace Layout{
		/*
		 * Border layout will have 5 regions
			 By default if no position is specified it will be placed in the center position
			 Folowing are regions available:

			 North  - at the top which will be layed out as BoxLayout with horizontal positioning
			 South  - at the bottom which will be layed out as BoxLayout with horizontal positioning
			 West   - at the left which will be layed out as BoxLayout with vertical positionioning
			 East   - at the right which will be layed out as BoxLayout with vertical positionong
			 Center - at the center and will be streched to fill all space left with vertical positioning

			 Reset behaviour: what will happen when you swap from other layout manager to border layout
			 
			 If there is 1 child in block children list it will be placed in center position
			 If there is more than 1 child then last child will be in center position and every other will be hidden

 		 */
		class BorderLayout : public ILayout
		{
		protected:
			IBlock * m_pSouthBlock;
			IBlock * m_pNorthBlock;
			IBlock * m_pWestBlock;
			IBlock * m_pEastBlock;
			IBlock * m_pCenterBlock;

		public:

			enum class Position{
				South,
				East,
				West,
				North,
				Center	
			};

			BorderLayout(IBlock * b);
			bool 
			invalidate() override;

			void 
			reset() override;
			
			/**
			 * Method draws from starting coordinates
			 */
			void 
			drawLayout() override;

			void 
			setPositioning(int p) override;

			/*
			 * Method to add layout to block
 	 		 */
			void
			addBlock(IBlock * block) override;

			/*
			 * Method to add layout to block
 	 		 */
			void
			addBlock(IBlock * block, BorderLayout::Position position);
		};	
	}
}
#endif
