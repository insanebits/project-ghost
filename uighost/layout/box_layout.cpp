#include "box_layout.h"
#include "ui_api.h" 
#include <stdio.h>
#include "ui_rect.h"
//#include "layout_exception.h" // unused
using namespace UI;//todo remove this
using namespace UI::Layout;

BoxLayout::BoxLayout(UI::IBlock * block, int positioning) : ILayout(block)
{
	//this->block = block;
	this->positioning = positioning;
}

BoxLayout::BoxLayout(UI::IBlock * block) : ILayout(block)
{
	//this->block = block;
	this->positioning = BoxLayout::HORIZONTAL;
}

/*rewritten method
 * remeber : children cannot resize parent
 * 
*/
bool BoxLayout::invalidate()
{
	int neededWidth = 0;
	int neededHeight = 0;

	//int calculatedWidth = 0; // calculated block size includes margin padding border and content
	//int calculatedHeight = 0;

	UIRect childPosition;
	UIRect parentPosition;

	Margin  childMargin;
	Padding childPadding;
	Border  childBorder;	

	// calculating parent width
	UIRect drawableArea; // where children can be positioned
	UI::IBlock * pBlock = this->block;
	
	if(!pBlock)
	{
		return false;
	}

	UIRect blockPosition = pBlock->getPosition(); // where block is located
	UI::Insets blockInsets = pBlock->getInsets();

	// starting coordinate x plus top inset
	drawableArea.coordX = blockPosition.coordX + blockInsets.getLeftInset();
	// starting coordinate y plus top inset
	drawableArea.coordY = blockPosition.coordY + blockInsets.getTopInset();
	// width minus insets
	drawableArea.width = blockPosition.width - blockInsets.getHorizontalInset();
	// height minus insets
	drawableArea.height = blockPosition.height - blockInsets.getVerticalInset();

	parentPosition = pBlock->getPosition();
	//printf("BoxLayout::%s() parent block size %s \n", __func__, parentPosition.toString().c_str());

	std::list<IBlock *> * children = pBlock->getChildren();
	if((children != nullptr)&&(children->size() > 0))
	{
		//aligning by horizontal axis
		if(this->positioning == BoxLayout::HORIZONTAL)
		{
			for(std::list<IBlock *>::iterator it  = children->begin(); it != children->end(); it++)
			{ 
				UI::Insets childInsets = (*it)->getInsets();

				// actual sizes including insets
				int childWidth = (*it)->getPreferredSize().width + childInsets.getHorizontalInset();
				int childHeight = (*it)->getPreferredSize().height + childInsets.getVerticalInset();

				neededWidth += childWidth; // adding to total needed width

				if(neededHeight < childHeight)
				{
					neededHeight = childHeight;
				} 				
			}
		} else { 
			// calculating alignment by vertical axis
			for(std::list<IBlock *>::iterator it  = children->begin(); it != children->end(); it++)
			{
				UI::Insets childInsets = (*it)->getInsets();
				// actual sizes including insets
				int childWidth = (*it)->getPreferredSize().width + childInsets.getHorizontalInset();
				int childHeight = (*it)->getPreferredSize().height + childInsets.getVerticalInset();
				
				neededHeight += childHeight; // adding to total needed width

				if(neededWidth < childWidth)
				{
					neededWidth = childWidth;
				} 
			}
		}// end of needed size calculation

		// setting children position size that means how much each child can expand itself from starting coordinate
		// this is needed before resizing because children does not have its positions
		//aligning by horizontal axis
		if(this->positioning == BoxLayout::HORIZONTAL)
		{
			for(std::list<IBlock *>::iterator it  = children->begin(); it != children->end(); it++)
			{
				childPosition = (*it)->getPosition();
				UI::Insets childInsets = (*it)->getInsets();

				int childWidth = (*it)->getPreferredSize().width;
				int childHorizontalInsets = childInsets.getHorizontalInset();

				
				//int childHeight = (*it)->getPreferredSize().height; // dont forget padding, margin and border	
				int childVerticalInsets = childInsets.getVerticalInset();
				
				childPosition.width = childWidth + childHorizontalInsets;
				// if child can space to required height
				if(((*it)->getMaximumSize().height + childVerticalInsets) >= neededHeight) // attention calculated width not actual content size(which is Block::maximumSize)
				{
					childPosition.height   = neededHeight; //can space to required height
				} else {
					childPosition.height   = (*it)->getMaximumSize().height + childVerticalInsets; // we will resize later
				}

				(*it)->setPosition(childPosition); // putting back updated position
			}
		} else { 
			// calculating alignment by vertical axis
			for(std::list<IBlock *>::iterator it  = children->begin(); it != children->end(); it++)
			{
				childPosition = (*it)->getPosition();
				UI::Insets childInsets = (*it)->getInsets();

				//int childWidth = (*it)->getPreferredSize().width;
				int childHorizontalInsets = childInsets.getHorizontalInset();

				
				int childHeight = (*it)->getPreferredSize().height; // dont forget padding, margin and border	
				int childVerticalInsets = childInsets.getVerticalInset();
				
				childPosition.height = childHeight + childVerticalInsets;
				// if child can space to required height
				if(((*it)->getMaximumSize().width + childVerticalInsets) >= neededWidth) // attention calculated width not actual content size(which is Block::maximumSize)
				{
					childPosition.width   = neededWidth; //can space to required width
				} else {
					childPosition.width   = (*it)->getMaximumSize().height + childHorizontalInsets; // we will resize later
				}

				(*it)->setPosition(childPosition); // putting back updated position
			}
		}	
		// check if children will fit into parent
		if((neededWidth < parentPosition.width) && (neededHeight < parentPosition.height))
		{
			// if children wont fit horizontally into preferred size we need to resize children
			if(neededWidth > parentPosition.width)
			{			
				this->resizeChildrenHorizontally(neededWidth, neededHeight);
	
			}
			// if children wont fit vertically into preferred size we need to resize children
			if(neededHeight > parentPosition.height )
			{
				this->resizeChildrenVertically(neededWidth, neededHeight);
			}
    
		} else {
			printf("BoxLayout::%s() children wont fit because children exceeds parent max size\n", __func__);	
		}		
		// finally set correct coordinates for children
		//std::cout << "Parent position:"<< this->block->getPosition()->toString() << std::endl;
		//this->setChildrenPosition(parentPosition);
		this->setChildrenPosition(drawableArea); // where we can actualy paint
	} else {
		//block has no children so it's width and height will be set to preferred by its parent 
	}

	return true;
}

void BoxLayout::setChildrenPosition(UIRect parentPosition)
{
	// setting start position
	int positionX = parentPosition.coordX;
	int positionY = parentPosition.coordY;
	UIRect childPosition;
	std::list<IBlock *> * children = this->block->getChildren();
	
	if(this->positioning == BoxLayout::HORIZONTAL)
	{
		for(std::list<IBlock *>::iterator it  = children->begin(); it != children->end(); it++)
		{
			childPosition = (*it)->getPosition();
			childPosition.coordX = positionX;
			childPosition.coordY = positionY;

			(*it)->setPosition(childPosition); // putting back updated position

			// pushing position by childPosition
			positionX += childPosition.width;				
		}
	} else { 
		// calculating alignment by vertical axis
		for(std::list<IBlock *>::iterator it  = children->begin(); it != children->end(); it++)
		{
			childPosition = (*it)->getPosition();
			childPosition.coordX = positionX;
			childPosition.coordY = positionY;
			(*it)->setPosition(childPosition); // putting back updated position
			// pushing position by childPosition
			positionY += childPosition.height;
		}
	}
}

bool BoxLayout::resizeChildrenVertically(int requiredWidth, int requiredHeight)
{
	Margin  childMargin;
	Padding childPadding;
	Border  childBorder;

	std::list<IBlock *> * children = this->block->getChildren();

	int overHeight  = this->block->getPreferredSize().height - requiredHeight;// size we need to resize children
	int resizeableChildren = children->size(); // how many children can be resized
		
	int heightPerChild = ceil(overHeight / resizeableChildren); // width we need to resize each child
	
	//bool resized = true; // to break infinite loop
	// we are not sure that first iteration will resize to needed size
	// must be careful for infinite loop
	while((overHeight > 0) && (resizeableChildren > 0))
	{

		int heightUnder = 0; // height we wasnt able to resize
		for(std::list<IBlock *>::iterator it  = children->begin(); it != children->end(); it++)
		{
			UIRect childPosition = (*it)->getPosition(); // holds child position
			UI::Insets childInsets = (*it)->getInsets();
			
			int childVerticalInsets = childInsets.getVerticalInset();
		
			// child can resize from current calculated to min value
			int childCanResize = childPosition.height - (*it)->getMinimumSize().height;
			//insets cannot be resized
			childCanResize -= childVerticalInsets;

			// child cannot resize enough
			if(heightPerChild > childCanResize)
			{
				//setting to smallest possible height
				childPosition.height   = (*it)->getMinimumSize().height  ;
				overHeight -= childCanResize;
				heightUnder += heightPerChild - childCanResize;
				if(resizeableChildren > 0)
				{
					resizeableChildren--; // we can resize one children less
				}
			} else {
				// child can successfully be resized
				childPosition.height   -= heightPerChild;
				overHeight -= heightPerChild;
			}
		}				
		heightPerChild = ceil(heightUnder / resizeableChildren);
	}
	return true;
}

bool BoxLayout::resizeChildrenHorizontally(int requiredWidth, int requiredHeight)
{
	Margin  childMargin;
	Padding childPadding;
	Border  childBorder;

	std::list<IBlock *> * children = this->block->getChildren();
	int overWidth  = this->block->getPosition().width - requiredWidth;// size we need to resize children
	int resizeableChildren = children->size(); // how many children can be resized
	int widthPerChild = ceil(overWidth / resizeableChildren); // width we need to resize each child
	// we are not sure that first iteration will resize to needed size
	// must be careful for infinite loop
	while((overWidth > 0) && (resizeableChildren > 0))
	{
		int widthUnder = 0; // width we wasnt able to resize
		for(std::list<IBlock *>::iterator it  = children->begin(); it != children->end(); it++)
		{
			UIRect childPosition = (*it)->getPosition(); // holds child position
			UI::Insets childInsets = (*it)->getInsets();

			int childHorizontalInsets = childInsets.getHorizontalInset();

			// child can resize from current calculated to min value
			int childCanResize = childPosition.width - (*it)->getMinimumSize().width;
			childCanResize -= childHorizontalInsets;

			// child cannot resize enough
			if(widthPerChild > childCanResize)
			{
				childPosition.width = (*it)->getMinimumSize().width;
				overWidth -= childCanResize;
				widthUnder += widthPerChild - childCanResize;
				if(resizeableChildren > 0)
				{
					resizeableChildren--; // we can resize one children less
				}
			} else {
				childPosition.width -= widthPerChild;
				overWidth -= widthPerChild;
			}
		}
		widthPerChild = ceil(widthUnder / resizeableChildren);
	}
	return true;
}

void 
BoxLayout::setPositioning(int p)
{
	positioning = p;
}

void 
BoxLayout::drawLayout()
{
	std::list<IBlock *> * children = this->block->getChildren();
	this->block->draw();//drawing current block
	// drawing current block children

	if(children)
	{
		for(std::list<IBlock *>::iterator it  = children->begin(); it != children->end(); it++)
		{
			(*it)->getLayout()->drawLayout();
		}	
	}
}

void
BoxLayout::addBlock(IBlock * block)
{
	if(this->block)
	{
		this->block->addChild(block);
	} else {
		printf("BoxLayout::%s() could not get block\n", __func__);	
	}
}


void BoxLayout::reset()
{
	// nothing to do so far because box layout only uses block children
}


