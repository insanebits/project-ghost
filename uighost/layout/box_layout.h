#ifndef BOX_LAYOUT_H
#define BOX_LAYOUT_H

#include <list>
#include <math.h>
#include <iostream>
#include "ui_rect.h"
#include "ILayout.h"

/**
 * This class manages how box will be placed on screen
 * Box Layout will be aligned by horizontal or vertical axis
 * It will place elements in smallest possible box
 */
namespace UI{
	//class UI::IBlock; // forward definition
	namespace Layout{

		class BoxLayout : public ILayout{
		public:
			enum Positioning{
				HORIZONTAL = 0,
				VERTICAL = 1
			};
			/**
			 * Method calculates block size
			 * This should be used after event which will cause block to change it's size
			 * TODO calculate spacing
			 * TODO calculate exact position on window
			 */		
			bool 
			invalidate() override;
			
			void 
			setPositioning(int p) override;
			/**
			 * Method draws from starting coordinates
			 */
			void 
			drawLayout() override;

			void
			reset() override;

			void
			addBlock(IBlock * block) override;

			BoxLayout(IBlock * block, int positioning);
			BoxLayout(IBlock * block);
		protected:
			//IBlock * block;// see IBlock::block
			
			/**
			 * Inner method used by invalidate
			 * Method tries to resize it's children to some wanted size
			 * If resizing was unsuccessfull block will have closest possible size to required
			 * Parameters
			 * block - block to resize
			 * wantedWidth - width to resize to
			 * wantedHeight - height to resize to
			 * sizeDefect - how much we was short to wanted size 
			 * negative values mean that block was too small by
			 */
			// not used
			//bool tryResize(Block * block, int wantedWidth, int wantedHeight, UIRect &sizeDefect)
			/**
			 * Method will calculate prefered size of block
			 * it will be size how normaly all components want's to appear
			 * when they cannot fit inside they will be resized
			 */
			UIRect calculatePreferredSize();
			// inner methods
			void setChildrenPosition(UIRect parentPosition);
			bool resizeChildrenVertically(int requiredWidth, int requiredHeight);
			bool resizeChildrenHorizontally(int requiredWidth, int requiredHeight);

		};
	}
}
#endif
