#include <exception>

#ifndef LAYOUT_EXCEPTION_H
#define LAYOUT_EXCEPTION_H
namespace UI
{
	namespace Layout{
		class LayoutException: public std::exception
		{
		
		public:
			enum class LayoutExceptionType{
				MAX_BOUND,
				MIN_BOUND
			}
			
			LayoutExceptionType exceptionType;
			
			LayoutException(LayoutExceptionType type)
			{
				exceptionType = type;
			}
			
			
		};
	}
}
#endif
