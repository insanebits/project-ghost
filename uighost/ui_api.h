//Source by Robertas Jašmontas
#ifndef UI_API_H
#define UI_API_H
#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h>//printf
#include <math.h>

#include <boost/filesystem.hpp>
#include <stdio.h>

#include <GL/glew.h>// glew must be first
#include <GL/gl.h> // openGL

#include "GHOST_ISystem.h" // GHOST

#include "STR_String.h"//TODO remove
#include "GHOST_Rect.h"

#include "GHOST_IEvent.h"
#include "GHOST_IEventConsumer.h"
#include "UI_interface.h"
#include "BLI_math.h"

#include "event_api.h"// should it be before window?
#include "ui_window.h"// must be before block api which includes IBlock that forwards window
// Custom includes
#include "block_api.h"
#include "box_layout.h"
// event system

//#include "key_driver.h"
//#include "mouse_driver.h"
//#include "key_listener.h"
//#include <boost/signals2/signal.hpp>

#include "fontlib/font_api.h"
#include "fontlib/font_manager.h"

#include "ui_drawing.h"
#include "ui_window_manager.h"

#include "label.h"
#include "fps_label.h"
#include "toggle_button.h"
#include "textfield.h"
#include "text_block.h"
#include "image_block.h"
//#include "ILayout.h"
#include "border_layout.h"




#define LEFT_EYE  0
#define RIGHT_EYE 1

namespace UI{
	static void timer(GHOST_ITimerTask *task, GHOST_TUns64 time);

	static class Application * fApp;
	static GHOST_ISystem *fSystem = 0;
	static FontManager * fm;
	static Events::KeyDriverPtr keyDriver;
	//static Events::MouseDriverPtr mouseDriver;
	static Events::IKeyListenerPtr kl1;
	static Events::IKeyListenerPtr kl2;
	
	static Events::EventManagerPtr pEventManager;
	static Events::EventManagerWPtr wpEventManager;
	
	class Application : public GHOST_IEventConsumer {
	public:
		Application(GHOST_ISystem *system);
		~Application(void);
		virtual bool processEvent(GHOST_IEvent *event);
		UI::Window * customWindow;
		UI::Window * customWindow2;

		GHOST_ISystem *m_system;
		GHOST_ITimerTask *m_timer;
		UI::WindowManager * pWindowManager;
		FontManagerPtr pFontManager;

		GHOST_TStandardCursor m_cursor;
		bool m_exitRequested;
		bool stereo;
		void initResources();
		void createLayout();
	};
}// end of namespace


#endif
