#ifndef UI_BORDER_H
#define UI_BORDER_H

#include "ui_types.h" //for Color
#include "ui_rect.h" // for UIRect

namespace UI{	
	class Border {
	protected:
		int m_Thickness;
		Color m_Color;
		UIRect m_Position;
	public:
		// wont need to create file only for constructors
		Border(int thickness, Color color, UIRect position) : m_Thickness(thickness), m_Color(color), m_Position(position)
		{}

		Border(int thickness, Color color) : m_Thickness(thickness), m_Color(color)
		{}

		Border(int thickness) : m_Thickness(thickness), m_Color(Color(0,0,0))
		{}

		Border() : m_Thickness(0), m_Color(Color(0,0,0))
		{}

		int
		getThickness(){return m_Thickness;}
		
		Color
		getColor(){return m_Color;}

		void 
		setPosition(UIRect position){m_Position = position;}
	};
}

#endif
