#include "ui_drawing.h"
/*void 
drawBox(int mode, float minx, float miny, float maxx, float maxy, float rad)
{
	drawBox(mode, 640,480, minx, miny, maxx, maxy,rad);
}*/

void 
drawBox(int mode, int sw, int sh, int minx, int miny, int maxx, int maxy)
{
	drawBox(mode, sw, sh, minx, miny, maxx, maxy, 0);
}

void 
drawBox(int mode, int sw, int sh, int minx, int miny, int maxx, int maxy, float rad)
{
	//printf("%s() window size %dx%d drawing at %d %d to %d %d \n", __func__, sw, sh, minx, miny, maxx, maxy);
	float hpx = 2.0/sw;
	float hpy = 2.0/sh;

	//printf("Given x1 %f y1 %f x2 %f y2 %f\n", minx, miny, maxx, maxy);
	//printf("hpx %f hpy %f \n", hpx, hpy);
	float x1 = -1.0 + minx*hpx;
	float y1 = 1.0 - miny*hpy;
	float x2 = -1.0 + maxx*hpx;
	float y2 = 1.0 - maxy*hpy;
	//printf("%s() block top left %f %f\n", __func__, x1, y1);

	//printf("x1 %f y1 %f x2 %f y2 %f\n", x1, y1, x2, y2);

	glBegin(mode);
			glVertex2f(x1, y2); // The bottom left corner  
			glVertex2f(x1, y1); // The top left corner  
			glVertex2f(x2, y1); // The top right corner  
			glVertex2f(x2, y2); // The bottom right corner  
	glEnd();

	//drawBorder(mode, sw, sh, minx, miny, maxx, maxy, 3);
}

void 
UI::drawBorder(int sw, int sh, UI::UIRect position, int thickness)
{
	drawBorder(GL_POLYGON, sw, sh, position.coordX, position.coordY, position.coordX + position.width, position.coordY + position.height, thickness);
}

void 
UI::drawBorder(int mode, int sw, int sh, int minx, int miny, int maxx, int maxy, int thickness)
{
	//printf("%s() window size %dx%d drawing at %d %d to %d %d \n", __func__, sw, sh, minx, miny, maxx, maxy);
	float hpx = 2.0/sw;
	float hpy = 2.0/sh;

	float x1;
	float y1;
	float x2;
	float y2;

	if(thickness <= 0)
	{
		return; // nothing to paint	
	}
	//printf("Given x1 %f y1 %f x2 %f y2 %f\n", minx, miny, maxx, maxy);
	//printf("hpx %f hpy %f \n", hpx, hpy);
	x1 = -1.0 + minx*hpx;
	y1 = 1.0 - miny*hpy;
	x2 = -1.0 + maxx*hpx;
	y2 = 1.0 - (miny + thickness)*hpy;


	// top border
	glBegin(mode);
			glVertex2f(x1, y2); // The bottom left corner  
			glVertex2f(x1, y1); // The top left corner  
			glVertex2f(x2, y1); // The top right corner  
			glVertex2f(x2, y2); // The bottom right corner  
	glEnd();

	// right border
	x1 = -1.0 + (maxx - thickness)*hpx;
	//y1 = 1.0 - miny*hpy;
	//x2 = -1.0 + maxx*hpx;
	y2 = 1.0 - maxy*hpy;


	// top border
	glBegin(mode);
			glVertex2f(x1, y2); // The bottom left corner  
			glVertex2f(x1, y1); // The top left corner  
			glVertex2f(x2, y1); // The top right corner  
			glVertex2f(x2, y2); // The bottom right corner  
	glEnd();

// bottom border
	x1 = -1.0 + minx*hpx;
	y1 = 1.0 - (maxy - thickness)*hpy;
	//x2 = -1.0 + maxx*hpx;
	//y2 = 1.0 - maxy*hpy;


	// top border
	glBegin(mode);
			glVertex2f(x1, y2); // The bottom left corner  
			glVertex2f(x1, y1); // The top left corner  
			glVertex2f(x2, y1); // The top right corner  
			glVertex2f(x2, y2); // The bottom right corner  
	glEnd();

	// left border
	x1 = -1.0 + minx*hpx;
	y1 = 1.0 - miny*hpy;
	x2 = -1.0 + (minx + thickness)*hpx;
	y2 = 1.0 - maxy*hpy;


	// top border
	glBegin(mode);
			glVertex2f(x1, y2); // The bottom left corner  
			glVertex2f(x1, y1); // The top left corner  
			glVertex2f(x2, y1); // The top right corner  
			glVertex2f(x2, y2); // The bottom right corner  
	glEnd();
	

}

const float * 
decodeRGB(UI::Color * c)
{
	float * r = new float[3];
	r[0] = (c->red/255.0); 
	r[1] = (c->green/255.0);
	r[2] = (c->blue/255.0);
	return r;
}

float * 
decodeRGBA(UI::Color * c)
{
	float * r = new float[4]; // was float[3]; 
	r[0] = (c->red/255.0); 

	r[1] = (c->green/255.0);

	r[2] = (c->blue/255.0);

	r[3] = (c->alpha/100.0);	
	return r;
}

bool 
UI::drawBitmap(Bitmap * image, int sw, int sh, int minx, int miny, int maxx, int maxy)
{
	float hpx = 2.0/sw;
	float hpy = 2.0/sh;

	float x1 = -1.0 + minx*hpx;
	float y1 = 1.0 - miny*hpy;
	float x2 = -1.0 + maxx*hpx;
	float y2 = 1.0 - maxy*hpy;

  if(image==nullptr) {
		return false;
  }

	GLuint texture[5];

	glGenTextures(1, &texture[0]);

	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->data);
	// Draw a textured quad
	glEnable(GL_TEXTURE_2D);

	//float gx = 2.0/640.0; // x pixel size in gl
	//float gy = 2.0/480.0; // y pixel size in gl
	//float x1 = -1.0;
	//float y1 =  1.0;
	//float x2 = x1 + image->width * hpx;
	//float y2 = y1 - image->height * hpy;

	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_QUADS);

		glTexCoord2f(0.0f, 0.0f); glVertex3f(x1, y2, 0);
		glTexCoord2f(1.0f, 0.0f); glVertex3f( x2, y2, 0);
		glTexCoord2f(1.0f, 1.0f); glVertex3f( x2,  y1, 0);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(x1, y1,  0);
	glEnd();

  /*if (image) { // will be handled by image block
	delete image;
  }*/


/*---------------------------------------drawing part-------------------------------------------*/

  return true;
}
