#ifndef DRAWING_H
#define DRAWING_H
#include <GL/gl.h>
#include <stdio.h>
#include "uighost_macros.h"
#include "ui_types.h"
#include "ui_rect.h"
#include "drawing/Bitmap.h"
// todo add other functions to UI namespace
void drawBox(int mode, int sw, int sh, int minx, int miny, int maxx, int maxy, float rad);
void drawBox(int mode, int sw, int sh, int minx, int miny, int maxx, int maxy);
namespace UI{
	void 
	drawBorder(int sw, int sh, UI::UIRect position, int thickness);
	void 
	drawBorder(int mode, int sw, int sh, int minx, int miny, int maxx, int maxy, int thickness);
	bool 
	drawBitmap(Bitmap * image, int sw, int sh, int minx, int miny, int maxx, int maxy);
}
const float * decodeRGB(UI::Color * c);
float * decodeRGBA(UI::Color * c);

const int TF_NONE = 0;
const int TF_BILINEAR = 1;

void LoadBitmapTexture(int id);
void SetTextureFilter(int newfilter);


#endif
