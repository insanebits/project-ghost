#include "ui_insets.h"
using namespace UI;

Insets::Insets()
{}

Insets::Insets(Margin margin) : m_Margin(margin)
{}

Insets::Insets(Margin margin, Border border) : m_Margin(margin), m_Border(border) 
{}

Insets::Insets(Margin margin, Border border, Padding padding) : m_Margin(margin), m_Border(border), m_Padding(padding)
{}

Margin
Insets::getMargin()
{
	return m_Margin;
}

Padding
Insets::getPadding()
{
	return m_Padding;
}

Border
Insets::getBorder()
{
	return m_Border;
}

int
Insets::getLeftInset()
{
	int inset =  m_Border.getThickness() + m_Margin.getLeftMargin() + m_Padding.getLeftPadding();
	return inset;
}

int 
Insets::getTopInset()
{
	int inset =  m_Border.getThickness() + m_Margin.getTopMargin() + m_Padding.getTopPadding();
	return inset;
}

int 
Insets::getRightInset()
{
	int inset =  m_Border.getThickness() + m_Margin.getRightMargin() + m_Padding.getRightPadding();
	return inset;
}

int 
Insets::getBottomInset()
{
	int inset =  m_Border.getThickness() + m_Margin.getBottomMargin() + m_Padding.getBottomPadding();
	return inset;
}

int 
Insets::getHorizontalInset()
{
	// 2* border because border is in both sides
	int inset = 2 * m_Border.getThickness() + m_Margin.getLeftMargin() + m_Padding.getLeftPadding();
	inset += m_Margin.getRightMargin() + m_Padding.getRightPadding();
	return inset;
}

int 
Insets::getVerticalInset()
{
	// 2* border because border is in both sides
	int inset =  2 * m_Border.getThickness() + m_Margin.getTopMargin() + m_Padding.getTopPadding();
	inset += m_Margin.getBottomMargin() + m_Padding.getBottomPadding();
	return inset;
}

void 
Insets::setBorder(Border border)
{
	m_Border = border;
}

void
Insets::setMargin(Margin margin)
{
	m_Margin = margin;
}

void 
Insets::setPadding(Padding padding)
{
	m_Padding = padding;
}	
