#ifndef UI_INSETS_H
#define UI_INSETS_H
#include "ui_types.h"
#include "ui_border.h"

namespace UI
{
	/*
	 * This class is used mainly in layouts for easier calculations on block sizes
	 * Padding is closer to inner block space inside border
	 * Margin is outter space outside border which not include background
	 */
	class Insets 
	{
	private:
	protected:
		Margin m_Margin;
		Border m_Border;
		Padding m_Padding;
	public:

		Insets();
		Insets(Margin margin);
		Insets(Margin margin, Border border);
		Insets(Margin margin, Border border, Padding padding);

		Margin
		getMargin();

		Padding
		getPadding();

		Border
		getBorder();

		/*left side margin + padding + border*/		
		int
		getLeftInset();

		/*top side margin + padding + border*/		
		int 
		getTopInset();

		/*right side margin + padding + border*/		
		int 
		getRightInset();

		/*bottom side margin + padding + border*/		
		int 
		getBottomInset();

		/*left + right inset*/		
		int 
		getHorizontalInset();

		/*top + bottom insets*/		
		int 
		getVerticalInset();

		void 
		setBorder(Border border);

		void
		setMargin(Margin margin);

		void 
		setPadding(Padding padding);	
	};
}
#endif
