#include "ui_rect.h"
using namespace UI;

UIRect::UIRect()
{
	this->width = 0;
	this->height = 0;
	this->coordX = 0;
	this->coordY = 0;
}

UIRect::UIRect(int width, int height)
{
	this->width = width;
	this->height = height;
	this->coordX = 0;
	this->coordY = 0;
}

void UIRect::setWidth(int width)
{
	this->width = width;
}

void UIRect::setHeight(int height)
{
	this->height = height;
}

void UIRect::setSize(int width, int height)
{
	this->width = width;
	this->height = height;
}

int UIRect::getWidth()
{
	return this->width;
}

int UIRect::getHeight()
{
	return this->height;
}

bool UIRect::addMargin(Margin * margin)
{
	if(margin == nullptr)
	{
		return false;
	}
	
	this->height += margin->top + margin->bottom;
	this->width += margin->left + margin->right;	
	
	return true;
}

std::string UIRect::toString()
{
	std::stringstream s;
	s << " width: " << width << " height: " << height << " coordX: " << coordX << " coordY: " << coordY << std::endl;
	return s.str();
}

UIRect::UIRect(int w, int h, int x, int y) : width(w), height(h), coordX(x), coordY(y)
{}

