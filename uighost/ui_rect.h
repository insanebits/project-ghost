#include "ui_types.h"// for margin
#include <string>
#include <sstream>
#include <iostream>

#ifndef UI_RECT_H
#define UI_RECT_H
namespace UI
{
	class Margin; //forward
	class UIRect //TODO should be converted to simply rect UI::Rect will look nicer
	{
	public:
		int width;
		int height; 
		int coordX; // rect start X coordinate 
		int coordY; // rect start Y coordinate
		
		UIRect();
		UIRect(int width, int height);
		UIRect(int width, int height, int x, int y);

		void setWidth(int width); // possibly wont be used 
		void setHeight(int height); // possibly wont be used
		void setSize(int width, int height);
		int getWidth();
		int getHeight();
		bool addMargin(Margin * margin); // handy when calculating sizes
		std::string toString();
	};
}
#endif
