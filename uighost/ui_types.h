#include <iostream>
#include <string>
#include <sstream>
#include <map>
#ifndef UI_TYPES_H
#define UI_TYPES_H

//#include "ui_rect.h"
/*
 * There should be classes and types that are not independent from something
 * To be able to include them without worying about circular includes
 */
namespace UI{
	// Defines how will block expand
	class Spacing{
	public:
		enum {
			NONE = 0,
			HORIZONTAL = 2,
			VERTICAL = 4,
			TOP = 8,
			BOTTOM = 16,
			LEFT = 32,
			RIGHT = 64
		};
	};
	
	class MarginTypes{
	public:
		enum {
			VERTICAL=5,
			HORIZONTAL=10,
			TOP=1,
			RIGHT=2,
			BOTTOM=4,
			LEFT=8
		};
		
	};

	enum class Colors{
		Red,
		Black,
		White,
		Blue,
		Green,
		DarkGrey,
		LightGrey,
		Grey,
		Yellow,
		Purple,
		LightPurple,
		LightGreen,
		LightYellow
	};


	class Color
	{
	public:
		short red, green, blue, alpha;
		Color(short r, short g, short b): red(r), green(g), blue(b), alpha(100) {}
		Color(short r, short g, short b, short a): red(r), green(g), blue(b), alpha(a) {}
		Color(Colors c)
		{
			bool found = true;
                        alpha = 100; // by default
			switch(c)
			{
				case Colors::Red : {
					red=255; 
					green = 0; 
					blue = 0;
				}
				break;
				case Colors::Blue : 
				{
					red=0; 
					green = 0; 
					blue = 255; 
				}
				break;
				case Colors::Green : {
					red=0; 
					green = 255; 
					blue = 0;
				}
				break;
				case Colors::White : {
					red=255; 
					green = 255; 
					blue = 255; 

				}
				break;
				case Colors::Black : {
					red=0; 
					green = 0; 
					blue = 0;
				} 
				break;
				case Colors::Grey : {
					red=96; 
					green = 96; 
					blue = 96;
				}
				break;
				case Colors::LightGrey : {
					red=160; 
					green = 160; 
					blue = 160;
				} 
				break;
				case Colors::DarkGrey :{
					red=64; 
					green = 64; 
					blue = 64;
				} 
				break;
				case Colors::LightPurple : {
					red=255; 
					green = 102; 
					blue = 255;
				} 
				break;
				case Colors::Purple : {
					red=255; 
					green = 51; 
					blue = 255;
				} 
				break;
				case Colors::Yellow : {
					red=255; 
					green = 255; 
					blue = 51;
				} 
				break;
				case Colors::LightGreen : {
					red=153; 
					green = 255; 
					blue = 0;
				} 
				break;
				case Colors::LightYellow : {
					red=255; 
					green = 255; 
					blue = 102;
				} 
				break;
				default : found = false; break;
			}
			if(!found)
			{
		    red=0; 
		    green = 0; 
		    blue = 0;
			}
		}

		Color(): red(0), green(0), blue(0), alpha(100) {}
	};

	class Margin {
	public:
		int left;
		int right;
		int top;
		int bottom;
		// wont need to create file only for constructors
		Margin(int t, int r, int b, int l){top=t; right=r; bottom=b; left=l;}//constructor for all parameters
		Margin(int h, int v){top=v; bottom=v; left=h; right=h;}// constructor with horizontal and vertical margins
		Margin(int v){top=v; right=v; bottom=v; left=v;}// constructor for all margins
		Margin(){top=0; right=0; bottom=0; left=0;}// default constructor
		std::string toString(){std::stringstream ss; ss << " top: " << top << " right: " << right << " bottom: " << bottom << " left: " << left << std::endl; return ss.str();}
		int getLeftMargin() {return left;}
		int getTopMargin() {return top;}
		int getRightMargin() {return right;}
		int getBottomMargin() {return bottom;}

		void setLeftMargin(int m) {left = m;}
		void setTopMargin(int m) {top = m;}
		void setRightMargin(int m) {right = m;}
		void setBottomMargin(int m) {bottom = m;}
	};

	class Padding {
	public:
		int left;
		int right;
		int top;
		int bottom;
		// wont need to create file only for constructors
		Padding(int t, int r, int b, int l){top=t; right=r; bottom=b; left=l;}//constructor for all parameters
		Padding(int h, int v){top=v; bottom=v; left=h; right=h;}// constructor with horizontal and vertical margins
		Padding(int v){top=v; right=v; bottom=v; left=v;}// constructor for all margins
		Padding(){top=0; right=0; bottom=0; left=0;}// default constructor
		std::string toString(){std::stringstream ss; ss << " top: " << top << " right: " << right << " bottom: " << bottom << " left: " << left << std::endl; return ss.str();}

		int getLeftPadding() {return left;}
		int getTopPadding() {return top;}
		int getRightPadding() {return right;}
		int getBottomPadding() {return bottom;}

		void setLeftPadding(int m) {left = m;}
		void setTopMarginPadding(int m) {top = m;}
		void setRightMarginPadding(int m) {right = m;}
		void setBottomMarginPadding(int m) {bottom = m;}
	};
}
#endif
