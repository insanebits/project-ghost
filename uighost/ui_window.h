#ifndef UI_WINDOW_H
#define UI_WINDOW_H
#include <time.h>

#include "IBlock.h"
//#include "block.h" // removed using generic IBlock
#include "event_manager.h"
#include "GHOST_ISystem.h"
#include <boost/weak_ptr.hpp>
#include "font_api.h"
#include "mouse_driver.h"// might be dangrous
#include "key_driver.h"  // ---||----


namespace UI{
	// class types
	typedef boost::shared_ptr<GHOST_IWindow> GWindowPtr;
	
	
	class Window;
	typedef boost::shared_ptr<Window> WindowPtr; 
	typedef boost::weak_ptr<Window> WindowWPtr;// should be typedef'ed where it is used

	class Window{
	// parameters
	private:
		GWindowPtr m_pGWindow;
		IBlockPtr m_pBlock; // root block associated with window
		time_t m_LastRepaint;

		int m_WindowWidth;
		int m_WindowHeight;

		Events::EventManagerPtr m_pEventManager;
		Events::KeyDriverPtr m_pKeyDriver;
		Events::MouseDriverPtr m_pMouseDriver;
		FontManagerPtr m_pFontManager;

	public:
		//static WindowPtr Null; // nullptr for itself to check if null
	protected:


	// methods
	private:
		void
		init(){
			m_pEventManager.reset(new Events::EventManager()); // creating new Event manager for this window

			// weak pointer for drivers to prevent circular dependency
			Events::EventManagerWPtr wpEventManager = m_pEventManager;

			//creating keydriver
			m_pKeyDriver.reset(new Events::KeyDriver(wpEventManager, "KeyEvent"));
			Events::IEventProducerPtr pKeyEventProducer(boost::dynamic_pointer_cast<Events::IEventProducer>(m_pKeyDriver));			

			// creating mouse driver
			m_pMouseDriver.reset(new Events::MouseDriver(wpEventManager, "MouseEvent"));
			Events::IEventProducerPtr pMouseEventProducer(boost::dynamic_pointer_cast<Events::IEventProducer>(m_pMouseDriver));					

			// window events
			m_pEventManager->registerEventType("KeyEvent", pKeyEventProducer);
			m_pEventManager->registerEventType("MouseEvent", pMouseEventProducer);		
				}
	public:
		Window()
		{
			init();
		}

		Window(GHOST_IWindow * wnd) : m_pGWindow(wnd)
		{
			GHOST_Rect bnds;
			wnd->getClientBounds(bnds);

			setWindowBounds(bnds.getWidth(), bnds.getHeight());
			//m_pGWindow.reset(wnd);//assign GHOST window to a shared pointer	
			init();
		}

		/*this probably wont be used since block has to have window handle */
		/*Window(GHOST_IWindow * wnd, IBlockPtr block): m_pBlock(block), m_pGWindow(wnd)
		{
			//m_pGWindow.reset(wnd);//assign GHOST window to a shared pointer	
			//m_pBlock = block;
			init();
		}*/

		GWindowPtr getGWindow()
		{
			return m_pGWindow;	
		}

		IBlockPtr getBlock()
		{
			return m_pBlock;	
		}

		void setBlock(IBlockPtr pBlock)
		{
			m_pBlock = pBlock;
		}

		time_t getLastRepaint()
		{
			return m_LastRepaint;		
		}
		//TODO update function name... nothing better came to mind
		void updateRepaintTime()
		{
			time(&m_LastRepaint);
		}
		
		Events::KeyDriverPtr 
		getKeyDriver()
		{
			return m_pKeyDriver;		
		}

		Events::MouseDriverPtr 
		getMouseDriver()
		{
			return m_pMouseDriver;		
		}

		FontManagerPtr
		getFontManager(){
			return m_pFontManager;
		}

		Events::EventManagerPtr
		getEventManager(){
			return m_pEventManager;
		}

		void
		setFontManager(FontManagerPtr pFontManager){
			m_pFontManager = pFontManager;
		}

		void 
		setWindowBounds(int width, int height)
		{
			m_WindowWidth = width;
			m_WindowHeight = height;
		}

		int
		getWindowWidth()
		{
			return m_WindowWidth;
		}

		int
		getWindowHeight()
		{
			return m_WindowHeight;
		}
	
		/*converting screen coordinates to client*/
		void
		screenToClient(int sx, int sy, int & cx, int & cy)
		{
			m_pGWindow->screenToClient(sx, sy, cx, cy);
		}

	protected:
	};
}
#endif
