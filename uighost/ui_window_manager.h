#ifndef UI_WINDOW_MANAGER_H
#define UI_WINDOW_MANAGER_H
#include "ui_window.h"
#include <time.h>
#include "STR_String.h"

namespace UI{
	typedef boost::shared_ptr<UI::Window> WindowPtr;

	class WindowManager{
	//types
		typedef std::vector<WindowPtr> WindowList;

	//members
	public:
	protected:
		WindowList m_WindowList; // holds list of created windows
		GHOST_ISystem * m_pGSystem; // ghost system for creation and destruction of windows
		FontManagerPtr m_pFontManager;
	private:
	// methods
	public:
		WindowManager(GHOST_ISystem * pGSystem)
		{
			m_pGSystem = pGSystem;		
		}


		void
		setFontManager(FontManagerPtr pFontManager)
		{
			m_pFontManager = pFontManager;	
			// temporary
			// setting font manager for all children 
			// children should be able to get it by theirself
			vector<WindowPtr>::iterator it;
			for(it = m_WindowList.begin(); it != m_WindowList.end(); it++)
			{
				(*it)->setFontManager(m_pFontManager);
			}
		}
		
		FontManagerPtr
		getFontManager()
		{
			return m_pFontManager;		
		}		

		WindowPtr
		createWindow(std::string title, int x, int y, int w, int h){
			STR_String ghostTitle(title.c_str()); //workaround for GHOST_ISystem

			GHOST_IWindow * pGWindow = m_pGSystem->createWindow(ghostTitle, 
																													x, y, /*start coordinates*/
																													w, h, /*width and height*/
																													GHOST_kWindowStateNormal, /*normal window, not minimized*/
																													GHOST_kDrawingContextTypeOpenGL, 
																													false, false);

			WindowPtr window(new UI::Window(pGWindow));
			WindowWPtr wpWindow(window);
			// root block will be base type			
			IBlockPtr block(new Block(wpWindow, 
																UIRect(w, h),     /*preferred size*/
																UIRect(100, 100), /*minimum size*/
																UIRect(w, h),     /*maximum size*/
																Spacing::HORIZONTAL));	
			
			window->setBlock(block);
			m_WindowList.push_back(window);

			return window;
		}
		/**
		 * Method to get window from pointer to ghost window
		 */
		WindowPtr
		getWindow(GHOST_IWindow * window)
		{
			WindowList::iterator it;
			for(it = m_WindowList.begin(); it != m_WindowList.end(); it++)
			{
				if(window == (*it)->getGWindow().get())
				{
					return (*it);				
				}			
			}

			return WindowPtr(); // window does not exist
		}


		int
		updateWindow(GHOST_IWindow * window)
		{
			WindowPtr wnd = getWindow(window);
			if(!wnd){
				return 0;
			}
			
			wnd->updateRepaintTime();
			
			wnd->getGWindow()->activateDrawingContext();
			// clear screen first			
			glLoadIdentity();
			glClearColor(1.0, 1.0, 1.0, 0); // white
			glClear(GL_COLOR_BUFFER_BIT);		

			// we must set root position before invalidating children
			GHOST_Rect bnds;
			window->getClientBounds(bnds);	
                        
      // font manager requires window screen for rendering text
      //m_pFontManager->setWindowBounds(bnds.getWidth(), bnds.getHeight());
			// TODO remove this, font manager can get window bounds directly from window, or window can set it
      wnd->getFontManager()->setWindowBounds(bnds.getWidth(), bnds.getHeight());     
  		// setting window bounds for window
			wnd->setWindowBounds(bnds.getWidth(), bnds.getHeight());


			IBlockPtr pWindowBlock = wnd->getBlock();
			wnd->getBlock()->setPosition(UIRect(bnds.getWidth(), bnds.getHeight()));			
			wnd->getBlock()->invalidate();
			wnd->getBlock()->getLayout()->drawLayout();
			//UI::drawBitmap(bnds.getWidth(), bnds.getHeight(), 0,0,300,300); // test
			wnd->getGWindow()->swapBuffers();
			return 1;
		}

	void
	triggerQuenedEvents()
	{
			vector<WindowPtr>::iterator it;
			for(it = m_WindowList.begin(); it != m_WindowList.end(); it++)
			{
				(*it)->getEventManager()->triggerAllQuenedEvents();
			}
	}


	protected:
	private:

	};
}
#endif
